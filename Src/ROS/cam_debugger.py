#!/usr/bin/env python

import rospy
from sensor_msgs.msg import CompressedImage
from ackermann_msgs.msg import AckermannDriveStamped
from geometry_msgs.msg import TwistStamped
from cv_bridge import CvBridge
import time
import cv2
import math

TRUE_STEER_COLOR = (0, 255, 0)    # label arrow color
PRED_STEER_COLOR = (255, 255, 0)  # prediction arrow color
FPS_COLOR = (255, 0, 255)         # fps counter color

MIN_STEER_DEGREES = -90           # max left angle
MAX_STEER_DEGREES = 90            # max right angle

CAM_TOPIC = "/depth_camera/rgb/image_raw/compressed"
ENGINE_ACTION_TOPIC = "/engine_wrapper/output/default"
AI_ACTION_TOPIC = "/nr/ai/output/actions"

IMAGE_RESIZE_WIDTH = 1024

SIM_M_PER_SEC = 10.0


class CamDebugger:
    def __init__(self, show_image=True, verbose=False):
        self.cv_bridge = CvBridge()
        self.window_name = 'CameraStreamer'
        self.time = time.time()
        self.sec_to_avg_over = 1
        self.img_count = 0
        self.fps = 0.0
        self.show_image = show_image
        self.act_steer = 0.0
        self.act_speed = 0.0
        self.ai_steer = 0.0
        self.verbose = verbose
        self.record = False

    def cam_callback(self, msg):
        self.img_count += 1

        if self.show_image:
            image = self.cv_bridge.compressed_imgmsg_to_cv2(msg, desired_encoding="passthrough")

            width = image.shape[1]
            height = image.shape[0]

            image = cv2.resize(image, (IMAGE_RESIZE_WIDTH, height * IMAGE_RESIZE_WIDTH/width))

            width = image.shape[1]

            cv2.putText(image,
                        'FPS: {}'.format(self.fps),
                        (width-100, 20),
                        cv2.FONT_HERSHEY_PLAIN,
                        1.0, FPS_COLOR, 2)
            
            draw_speed_scale_on_img(image)
            draw_steer_scale_on_img(image)

            draw_speed_line_on_img(image, self.act_speed / SIM_M_PER_SEC, TRUE_STEER_COLOR)
            
            image = draw_arrow_on_img(image, self.act_steer, TRUE_STEER_COLOR)
            image = draw_arrow_on_img(image, self.ai_steer,  PRED_STEER_COLOR)

            cv2.imshow(self.window_name, image)
            key = cv2.waitKey(1)

            if key == ord("r"):
                self.record = True
            elif key == ord("e"):
                self.record = False

            if self.record:
                cv2.imwrite("record/{}.png".format(time.time()), image)

        if self.verbose:
            print("FPS: {}".format(self.fps))

        # calculate new fps value
        delta_time = time.time() - self.time
        if delta_time >= self.sec_to_avg_over:
            self.fps = round(self.img_count / delta_time, 2)
            self.img_count = 0
            self.time = time.time()

    def act_callback(self, msg):
        self.act_steer = msg.drive.steering_angle
        self.act_speed = msg.drive.speed

    def ai_callback(self, msg):
        self.ai_steer = msg.twist.angular.z

    def start(self):
        rospy.init_node('cam_debugger', anonymous=True)
        rospy.Subscriber(CAM_TOPIC, CompressedImage, self.cam_callback)
        rospy.Subscriber(ENGINE_ACTION_TOPIC, AckermannDriveStamped, self.act_callback)
        rospy.Subscriber(AI_ACTION_TOPIC,  TwistStamped,  self.ai_callback)
        rospy.spin()


def fmap(value, old_min, old_max, new_min, new_max):
    old_span = old_max - old_min
    new_span = new_max - new_min
    return (value - old_min) * new_span / old_span + new_min


def get_draw_coordinates(img, draw_border):
    width = img.shape[1]
    height = img.shape[0]
    
    draw_width  = int(width / 10)
    draw_height = int(height / 3)
    
    left   = width - draw_width - draw_border
    right  = width - draw_border
    middle = width - int(draw_width / 2) - draw_border
    
    top    = height - draw_height - draw_border
    bottom = height - draw_border
    
    return top, bottom, left, right, middle


def draw_arrow_on_img(image, angle, color):
    '''
    Draws an arrow with the specified angle on an image. Arrows are drawn from
    the bottom center of the image.
    :param image: image that arrows are to be drawn on to
    :param angle: angle of the arrow, relative to the images lower center
    :param color: color of the arrow
    :return: image with arrow drawn on top
    '''
    image_width = image.shape[1]
    image_height = image.shape[0]
    image_hor_center = int(image_width / 2)
    arrow_origin = (image_hor_center, image_height)
    arrow_length = image_height / 2

    steer_angle = fmap(angle, -1, 1, MIN_STEER_DEGREES, MAX_STEER_DEGREES)
    steer_angle_rad = math.radians(steer_angle)

    arrow_end = (
        image_hor_center + int(arrow_length * math.sin(steer_angle_rad)),
        image_height - int(arrow_length * math.cos(steer_angle_rad)))

    if color == TRUE_STEER_COLOR:
        cv2.putText(image, "TRUE VALUE", (5, 20), cv2.FONT_HERSHEY_PLAIN, 1, color, 2)
    elif color == PRED_STEER_COLOR:
        cv2.putText(image, "PREDICTION", (5, 40), cv2.FONT_HERSHEY_PLAIN, 1, color, 2)

    return cv2.arrowedLine(image, arrow_origin, arrow_end, color, 4)

def draw_speed_scale_on_img(img, draw_border=5):
    t, b, l, r, m = get_draw_coordinates(img, draw_border)
    
    WHITE = (255,255,255)
    
    # draw scale
    cv2.line(img, (m, b), (m, t), WHITE, 4)
    cv2.line(img, (l, t), (r, t), WHITE, 4)
    cv2.line(img, (l, b), (r, b), WHITE, 4)

def draw_steer_scale_on_img(img):
    image_width = img.shape[1]
    image_height = img.shape[0]
    image_hor_center = int(image_width / 2)
    arrow_origin = (image_hor_center, image_height)
    arrow_length = image_height / 2
    
    radius = int(arrow_length)
    axes = (radius, radius)
    startAngle = 0.0;
    endAngle = -180.0;
    WHITE = (255, 255, 255)
    cv2.ellipse(img, arrow_origin, axes, 0.0, startAngle, endAngle, WHITE, 2)

def draw_speed_line_on_img(img, speed, color, draw_border=5):
    t, b, l, r, m = get_draw_coordinates(img, draw_border)
    
    # draw speed line
    speed_height = b - int((b - t) * speed)
    cv2.line(img, (l, speed_height), (r, speed_height), color, 4)

def main():
    cam_debugger = CamDebugger(show_image=True, verbose=False)
    cam_debugger.start()


if __name__ == "__main__":
    main()
