class Speed:
    MAINTAIN = 0
    ACCELERATE = 1
    DEACCELERATE = 2
    
    @staticmethod
    def len():
        return 3