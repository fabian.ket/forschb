from tensorflow.compat.v1.keras.utils import Sequence
import math
import cv2
import numpy as np
import os
import yaml
import copy
import time
        
            
class BatchedTrainigSequenceGenerator(Sequence):

    def __init__(self, gens):
        self._gens = gens
        
    def __len__(self):
        max_len = 0
        
        for gen in self._gens:
            max_len = max(len(gen), max_len)
            
        return max_len
        
    def on_epoch_end(self):
        pass
        
    @staticmethod
    def _concat_ndarrays(a1, a2):
        
        result = None
        
        if a1 is None:
            result = a2
            
        elif a2 is None:
            result = a1
            
        else:
            result = np.concatenate((a1, a2), axis=0)
            
        return result
        
    def __getitem__(self, idx):
        
        result = []
        
        imgs = None
        pre_speeds = None
        pre_steers = None
        speeds = None
        steers = None
        speed_cost_weights = None
        steer_cost_weights = None
        
        for gen in self._gens:
            gen_imgs = None
            gen_pre_speeds = None
            gen_pre_steers = None
            gen_speeds = None
            gen_steers = None
            gen_speed_cost_weights = None
            gen_steer_cost_weights = None
        
            gen_output = gen[idx]
            
            gen_imgs = gen_output[0][0]
            
            if gen.return_speed_input:
                gen_pre_speeds = gen_output[0][1]
                
            if gen.return_steer_input:
                gen_pre_steers = gen_output[0][-1]
            
            if gen.return_speed_output:
                gen_speeds = gen_output[1][0]
                
            if gen.return_steer_output:
                gen_steers = gen_output[1][-1]
            
            if gen.return_speed_weights:
                gen_speed_cost_weights = gen_output[2][0]
                
            if gen.return_steer_weights:
                gen_steer_cost_weights = gen_output[2][-1]
                
                
            imgs = self._concat_ndarrays(imgs, gen_imgs)
            
            pre_speeds = self._concat_ndarrays(pre_speeds, gen_pre_speeds)
            pre_steers = self._concat_ndarrays(pre_steers, gen_pre_steers)
            
            speeds = self._concat_ndarrays(speeds, gen_speeds)
            steers = self._concat_ndarrays(steers, gen_steers)
            
            speed_cost_weights = self._concat_ndarrays(speed_cost_weights, gen_speed_cost_weights)
            steer_cost_weights = self._concat_ndarrays(steer_cost_weights, gen_steer_cost_weights)
        
        # INPUTS
        result_inputs = []
        
        # IMGS
        result_inputs.append(imgs)
        
        # SPEED INPUT
        if pre_speeds is not None:
            result_inputs.append(pre_speeds)
        
        # STEER INPUT
        if pre_steers is not None:
            result_inputs.append(pre_steers)
        
        # ADD INPUT TO RESULT
        result.append(result_inputs)
        
        # OUTPUTS
        result_outputs = []
        
        # SPEED OUTPUT
        if speeds is not None:
            result_outputs.append(speeds)
            
        # STEER OUTPUT
        if steers is not None:
            result_outputs.append(steers)
        
        # ADD OUTPUT TO RESULT
        result.append(result_outputs)
        
        # WEIGHTS
        result_weights = []
        
        # SPEED WEIGHT
        if speed_cost_weights is not None:
            result_weights.append(speed_cost_weights)
        
        # STEER WEIGHT
        if steer_cost_weights is not None:
            result_weights.append(steer_cost_weights)
        
        # ADD WEIGHT TO RESULT
        if speed_cost_weights is not None or steer_cost_weights is not None:
            result.append(result_weights)
        
        return result

class SequenceGeneratorRegression(Sequence):
    def __init__(self, dic, main_dir, img_processor, sequence_size=32, img_file_type="png", class_dist=None, class_key=None):
        
        self.dic = dic
        self.main_dir = main_dir
        
        self.img_processor = img_processor
        
        self.batch_size = 1
        self.sequence_size = sequence_size
        
        self.img_file_type = img_file_type
        
        self._last_speed = 0.0
        self._last_steer = 0.0
        
        self._img_shape = None
        
        self.return_img_input   = True
        self.return_speed_input = True        
        self.return_steer_input = True
        
        self.return_speed_output = True        
        self.return_steer_output = True
        
        self.return_speed_weights = False
        self.return_steer_weights = False
        
        self.data_list = []
        
        self._class_key = class_key
        
        self._minority_classes = []
        if class_dist is not None:
            dist_values = [len(dist) for dist in class_dist]
            
            max_class_idx = np.argmax(dist_values)
            max_class_len = dist_values[max_class_idx]
            max_class_len_half = max_class_len // 2
            
            for idx, value in enumerate(dist_values):
                if value < (max_class_len_half):
                    self._minority_classes.append(idx)
        
        self._init()

    def _init(self):
        self._get_data_list()

    def __len__(self):
        return math.ceil(len(self.data_list) / (self.batch_size * self.sequence_size))
        
    def on_epoch_end(self):
        pass

    def __getitem__(self, idx):
    
        result = []
        
        reshaped_imgs = []
        reshaped_pre_speeds = []
        reshaped_pre_steers = []
        reshaped_speeds = []
        reshaped_steers = []
        reshaped_speed_cost_weights = []
        reshaped_steer_cost_weights = []
        
        # zero padding
        if idx >= len(self) and self._img_shape is not None:
            
            # INPUTS
            reshaped_imgs = np.zeros((self.batch_size, self.sequence_size, self._img_shape[0], self._img_shape[1], self._img_shape[2]))
            reshaped_pre_speeds = np.zeros((self.batch_size, self.sequence_size, 1))
            reshaped_pre_steers = np.zeros((self.batch_size, self.sequence_size, 1))
            
            # OUTPUTS
            reshaped_speeds = np.zeros((self.batch_size, self.sequence_size, 1))
            reshaped_steers = np.zeros((self.batch_size, self.sequence_size, 1))
            
            # WEIGHTS
            reshaped_speed_cost_weights = np.zeros((self.batch_size, self.sequence_size, 1))
            reshaped_steer_cost_weights = np.zeros((self.batch_size, self.sequence_size, 1))
                        
        elif idx >= len(self) and self._img_shape is None:
            raise AttributeError("Internal image shape not set, call one existent idx")
        
        else:
            _, img_paths, speeds, steers, speed_classes, steer_classes, speed_cost_weights, steer_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])

            imgs = []
            
            pre_speeds = copy.deepcopy(speeds)
            pre_speeds = pre_speeds[:-1]
            pre_speeds = (self._last_speed,) + pre_speeds
            
            pre_steers = copy.deepcopy(steers)
            pre_steers = pre_steers[:-1]
            pre_steers = (self._last_steer,) + pre_steers

            augment_classes = speed_classes
            if self._class_key is "steering_class":
                augment_classes = steer_classes
            
            # image processing
            for img_path, augment_class in zip(img_paths, augment_classes):
                img = cv2.imread(img_path)
                
                augment = self.img_processor.augment
                
                idx_augment_class = np.argmax(augment_class)
                if self._class_key is not None and idx_augment_class in self._minority_classes:
                    self.img_processor.augment = True
                    
                img = self.img_processor.process(img)
                
                self.img_processor.augment = augment
                
                imgs.append(img)
                
                if self._img_shape is None:
                    self._img_shape = img.shape
            
            # COUNT DATA TO ADD FOR ZERO PADDING
            min_batch_size = self.batch_size * self.sequence_size
            imgs_len = len(imgs)
            data_to_add = 0
            
            if min_batch_size > imgs_len:
                data_to_add = min_batch_size - imgs_len
            
            
            imgs = list(imgs)
            pre_speeds = list(pre_speeds)
            pre_steers = list(pre_steers)
            speeds = list(speeds)
            steers = list(steers)
            speed_cost_weights = list(speed_cost_weights)
            steer_cost_weights = list(steer_cost_weights)
            
            self._last_speed = speeds[-1]
            self._last_steer = steers[-1]
            
            # zero padding
            for _ in range(data_to_add):
                imgs.append(np.zeros_like(imgs[0]))
                pre_speeds.append(0.0)
                pre_steers.append(0.0)
                speeds.append(0.0)
                steers.append(0.0)
                speed_cost_weights.append(0.0)
                steer_cost_weights.append(0.0)

            # imgs = np.asarray(imgs)
            
            for i in range(self.batch_size):
                reshaped_imgs.append(np.array(imgs[i*self.sequence_size:(i+1)*self.sequence_size]))
                
                reshaped_pre_speeds.append(  np.array([np.array([v]) for v in pre_speeds[i*self.sequence_size   : (i+1)*self.sequence_size]]))
                reshaped_pre_steers.append(  np.array([np.array([v]) for v in pre_steers[i*self.sequence_size   : (i+1)*self.sequence_size]]))
                
                reshaped_speeds.append(      np.array([np.array([v]) for v in speeds[i*self.sequence_size       : (i+1)*self.sequence_size]]))
                reshaped_steers.append(      np.array([np.array([v]) for v in steers[i*self.sequence_size       : (i+1)*self.sequence_size]]))
                
                reshaped_speed_cost_weights.append(np.array([v for v in speed_cost_weights[i*self.sequence_size : (i+1)*self.sequence_size]]))
                reshaped_steer_cost_weights.append(np.array([v for v in steer_cost_weights[i*self.sequence_size : (i+1)*self.sequence_size]]))
            
        
        # INPUTS
        result_inputs = []
        
        # IMGS
        result_inputs.append(np.asarray(reshaped_imgs))
        
        # SPEED INPUT
        if self.return_speed_input:
            result_inputs.append(np.asarray(reshaped_pre_speeds))
        
        # STEER INPUT
        if self.return_steer_input:
            result_inputs.append(np.asarray(reshaped_pre_steers))
        
        # ADD INPUT TO RESULT
        result.append(result_inputs)
        
        # OUTPUTS
        result_outputs = []
        
        # SPEED OUTPUT
        if self.return_speed_output:
            result_outputs.append(np.asarray(reshaped_speeds))
        
        # STEER OUTPUT
        if self.return_steer_output:
            result_outputs.append(np.asarray(reshaped_steers))
        
        # ADD OUTPUT TO RESULT
        if self.return_speed_output or self.return_steer_output:
            result.append(result_outputs)
        
        # WEIGHTS
        result_weights = []
        
        # SPEED WEIGHT
        if self.return_speed_weights:
            result_weights.append(np.asarray(reshaped_speed_cost_weights))

        # STEER WEIGHT
        if self.return_steer_weights:
            result_weights.append(np.asarray(reshaped_steer_cost_weights))
        
        # ADD WEIGHT TO RESULT
        if self.return_speed_weights or self.return_steer_weights:
            result.append(result_weights)
            
        return result


    def _get_data_list(self):
        
        data_list = []
        
        for key in self.dic.keys():
            img_name = key
            if "img_name" in self.dic[key].keys():
                img_name = self.dic[key]["img_name"]
        
            path = self.main_dir
            if "img_path" in self.dic[key].keys():
                path = self.dic[key]["img_path"]
        
            img_path = "{}/{}.{}".format(path, img_name, self.img_file_type)
            speed = self.dic[key]["speed"]
            steer = self.dic[key]["steering"]
            speed_class = self.dic[key]["speed_class"]
            steer_class = self.dic[key]["steering_class"]
            sp_cost_weight = self.dic[key]["speed_cost_weight"]
            st_cost_weight = self.dic[key]["steer_cost_weight"]
            data_list.append((key, img_path, speed, steer, speed_class, steer_class, sp_cost_weight, st_cost_weight))
        
        self.data_list = data_list
        

# ==========================================================================================
# ==========================================================================================
# ==========================================================================================
        
        
# class SequenceGeneratorSteerClassification(Sequence):
    # def __init__(self, dic, main_dir, img_processor, batch_size=16, sequence_size=32):
        
        # self.dic = dic
        # self.main_dir = main_dir
        
        # self.img_processor = img_processor
        
        # self.sequence_size = sequence_size
        # self.batch_size = batch_size
        
        # self.data_list = []
        
        # self._init()

    # def _init(self):
        # self._get_data_list()

    # def __len__(self):
        # return math.ceil(len(self.data_list) / (self.batch_size * self.sequence_size))
        
    # def on_epoch_end(self):
        # pass
        # # self.flip = not self.flip

    # def __getitem__(self, idx):
        
        # _, img_paths, steering_classes, steer_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # # TODO: not so nice workaround for bug below, if there are not batch_size * sequence_size
        # #       many images left, just use the batch before
        # if len(img_paths) < (self.batch_size * self.sequence_size):
            # idx -= 1
            # _, img_paths, steering_classes, steer_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # imgs = []
        
        # n_steering_classes = len(steering_classes[0])
        
        # pre_steerings = copy.deepcopy(steering_classes)
        # pre_steerings = pre_steerings[:-1]
        # pre_steerings = (np.zeros(n_steering_classes),) + pre_steerings
        
        # # self.flip = not self.flip

        # # image processing
        # for img_path in img_paths:
            # img = cv2.imread(img_path)
            # img = self.img_processor.process(img)
            # imgs.append(img)
        
        # # if self.img_processor.flip:
        # #     steerings = [(steer*(-1)) for steer in steerings]
        
        
        # reshaped_imgs = []
        # reshaped_steerings = []
        
        # reshaped_pre_steers = []
        # reshaped_steer_cost_weights = []
        
        # imgs = np.asarray(imgs)
        
        # # TODO: wenn nicht mehr batch_size * sequence_size viele Bilder zur Verfuegung stehen
        # #       werden einfach alle restlichen Bilder zu einer einzelnen Batch zusammengefasst
        # #       dies macht jedoch mit Keras Probleme, da hier bei RNNs die Batchsize nicht unterschiedlich
        # #       sein darf !!!!
        # if imgs.shape[0] < (self.batch_size * self.sequence_size):
            # reshaped_imgs.append(np.array(imgs))
            # reshaped_steerings.append(np.array([v for v in steering_classes]))
            
            # reshaped_pre_steers.append(np.array([v for v in pre_steerings]))
            # reshaped_steer_cost_weights.append(np.array([np.array(v) for v in steer_cost_weights]))
        # else:
            # for i in range(self.batch_size):
                # reshaped_imgs.append(np.array(imgs[i*self.sequence_size : (i+1)*self.sequence_size]))
                # reshaped_steerings.append(np.array([v for v in steering_classes[i*self.sequence_size : (i+1)*self.sequence_size]]))
                
                # reshaped_pre_steers.append(np.array([v for v in pre_steerings[i*self.sequence_size : (i+1)*self.sequence_size]]))
                # reshaped_steer_cost_weights.append(np.array([np.array(v) for v in steer_cost_weights[i*self.sequence_size : (i+1)*self.sequence_size]]))
                
        # return ([np.asarray(reshaped_imgs), np.asarray(reshaped_pre_steers)], [np.asarray(reshaped_steerings)], [np.asarray(reshaped_steer_cost_weights)])

    # def _get_data_list(self):
        
        # data_list = []
        
        # for key in self.dic.keys():
            # img_path = "{}/{}.png".format(self.main_dir, key)
            # steer_class = self.dic[key]["steering_class"]
            # cost_weight = self.dic[key]["steer_cost_weight"]
            
            # data_list.append((key, img_path, steer_class, cost_weight))
        
        # self.data_list = data_list
        
        
# class SequenceGeneratorSpeed(Sequence):
    # def __init__(self, dic, main_dir, img_processor, batch_size=16, sequence_size=32, img_file_type="png"):
        
        # self.dic = dic
        # self.main_dir = main_dir
        
        # self.img_processor = img_processor
        
        # self.sequence_size = sequence_size
        # self.batch_size = batch_size
        
        # self.img_file_type = img_file_type
        
        # self.return_weights = True
        
        # self.data_list = []
        
        # self._init()

    # def _init(self):
        # self._get_data_list()

    # def __len__(self):
        # if self.sequence_size == 0:
            # length = math.ceil(len(self.data_list) / (self.batch_size))
        # else:
            # length = math.ceil(len(self.data_list) / (self.batch_size * self.sequence_size))
        # return length
        
    # def on_epoch_end(self):
        # pass
        # # self.flip = not self.flip

    # def __getitem__(self, idx):
        
        # if self.sequence_size == 0:
            # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size : (idx+1) * self.batch_size])
        # else:
            # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # # TODO: not so nice workaround for bug below, if there are not batch_size * sequence_size
        # #       many images left, just use the batch before
        # if len(img_paths) < (self.batch_size * self.sequence_size):
            # idx -= 1
        
            # if self.sequence_size == 0:
                # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size : (idx+1) * self.batch_size])
            # else:
                # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # imgs = []
        
        # pre_speeds = copy.deepcopy(speeds)
        # pre_speeds = pre_speeds[:-1]
        # pre_speeds = (np.asarray([1.0, 0.0, 0.0]),) + pre_speeds
       
        
        # # self.flip = not self.flip

        # # image processing
        # for img_path in img_paths:
            # img = cv2.imread(img_path)
            # img = self.img_processor.process(img)
            # imgs.append(img)
        
        # if self.img_processor.flip:
            # steerings = [(steer*(-1)) for steer in steerings]
        
        
        # reshaped_imgs = []
        # reshaped_pre_speeds = []
        # reshaped_speeds = []
        # reshaped_speed_cost_weights = []
        
        # if self.sequence_size == 0:
            # reshaped_imgs = imgs
            # reshaped_speeds = pre_speeds
            # reshaped_speeds = speeds
            # reshaped_speed_cost_weights = speed_cost_weights
        # else:
            # imgs = np.asarray(imgs)
            
            # # TODO: wenn nicht mehr batch_size * sequence_size viele Bilder zur Verfuegung stehen
            # #       werden einfach alle restlichen Bilder zu einer einzelnen Batch zusammengefasst
            # #       dies macht jedoch mit Keras Probleme, da hier bei RNNs die Batchsize nicht unterschiedlich
            # #       sein darf !!!!
            # if imgs.shape[0] < (self.batch_size * self.sequence_size):
                # reshaped_imgs.append(np.array(imgs))
                # reshaped_pre_speeds.append(np.array([np.array([v]) for v in pre_speeds]))
                # reshaped_speeds.append(np.array([np.array([v]) for v in speeds]))
                # reshaped_speed_cost_weights.append(np.array([np.array([v]) for v in speed_cost_weights]))
            # else:
                # for i in range(self.batch_size):
                    # reshaped_imgs.append(np.array(imgs[i*self.sequence_size:(i+1)*self.sequence_size]))
                    
                    # reshaped_pre_speeds.append(  np.array([v for v in pre_speeds[i*self.sequence_size   : (i+1)*self.sequence_size]]))
                    # reshaped_speeds.append(      np.array([v for v in speeds[i*self.sequence_size       : (i+1)*self.sequence_size]]))
                    # reshaped_speed_cost_weights.append(np.array([v for v in speed_cost_weights[i*self.sequence_size : (i+1)*self.sequence_size]]))
        
        # if self.return_weights:
            # return ([np.asarray(reshaped_imgs), np.asarray(reshaped_pre_speeds)], [np.asarray(reshaped_speeds)], [np.asarray(reshaped_speed_cost_weights)])
            
        # else:
            # return ([np.asarray(reshaped_imgs), np.asarray(reshaped_pre_speeds)], [np.asarray(reshaped_speeds)])


    # def _get_data_list(self):
        
        # data_list = []
        
        # for key in self.dic.keys():
        
            # img_name = key
            # if "img_name" in self.dic[key].keys():
                # img_name = self.dic[key]["img_name"]
        
            # path = self.main_dir
            # if "img_path" in self.dic[key].keys():
                # path = self.dic[key]["img_path"]
        
            # img_path = "{}/{}.{}".format(path, img_name, self.img_file_type)
            # speed = self.dic[key]["speed_class"]
            # cost_weight = self.dic[key]["speed_cost_weight"]
            # data_list.append((key, img_path, speed, cost_weight))
        
        # self.data_list = data_list

# from collections import deque

# class SequenceGeneratorSpeedSeq(Sequence):
    # def __init__(self, dic, main_dir, img_processor, batch_size=1, sequence_size=32, img_file_type="png"):
        
        # self.dic = dic
        # self.main_dir = main_dir
        
        # self.img_processor = img_processor
        
        # self.sequence_size = sequence_size
        # self.batch_size = batch_size
        
        # self.img_file_type = img_file_type
        
        # self.return_weights = True
        
        # self.pre_queue_size = 50
        # self._pre_speed_queue = deque([0 for _ in range(self.pre_queue_size)], maxlen=self.pre_queue_size)
        
        
        # self.data_list = []
        
        # self._init()

    # def _init(self):
        # self._get_data_list()

    # def __len__(self):
        # if self.sequence_size == 0:
            # length = math.ceil(len(self.data_list) / (self.batch_size))
        # else:
            # length = math.ceil(len(self.data_list) / (self.batch_size * self.sequence_size))
        # return length
        
    # def on_epoch_end(self):
        # pass
        # # self.flip = not self.flip

    # def __getitem__(self, idx):
        
        # if idx == 0:
            # self._pre_speed_queue = deque([0 for _ in range(self.pre_queue_size)], maxlen=self.pre_queue_size)
        
        # if self.sequence_size == 0:
            # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size : (idx+1) * self.batch_size])
        # else:
            # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # # TODO: not so nice workaround for bug below, if there are not batch_size * sequence_size
        # #       many images left, just use the batch before
        # if len(img_paths) < (self.batch_size * self.sequence_size):
            # idx -= 1
        
            # if self.sequence_size == 0:
                # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size : (idx+1) * self.batch_size])
            # else:
                # _, img_paths, speeds, speed_cost_weights = zip(*self.data_list[idx * self.batch_size * self.sequence_size : (idx+1) * self.batch_size * self.sequence_size])
        
        # imgs = []
        
        # pre_speeds = copy.deepcopy(speeds)
        # pre_speeds = pre_speeds[:-1]
        # pre_speeds = (np.asarray([1.0, 0.0, 0.0]),) + pre_speeds
        
        # pre_speeds = []
        
        # tmp = copy.deepcopy(self._pre_speed_queue)
        
        # pre_speeds.append(tmp)
        
        # for speed in speeds[:-1]:
            # self._pre_speed_queue.append(speed)
            # pre_speeds.append(copy.deepcopy(self._pre_speed_queue))
            
        # pre_speeds = np.asarray(pre_speeds)
        
        # self._pre_speed_queue.append(speeds[-1])
        
        # # self.flip = not self.flip

        # # image processing
        # for img_path in img_paths:
            # img = cv2.imread(img_path)
            # img = self.img_processor.process(img)
            # imgs.append(img)
        
        # if self.img_processor.flip:
            # steerings = [(steer*(-1)) for steer in steerings]
        
        
        # reshaped_imgs = []
        # reshaped_pre_speeds = []
        # reshaped_speeds = []
        # reshaped_speed_cost_weights = []
        
        # imgs = np.asarray(imgs)
        
        # # TODO: wenn nicht mehr batch_size * sequence_size viele Bilder zur Verfuegung stehen
        # #       werden einfach alle restlichen Bilder zu einer einzelnen Batch zusammengefasst
        # #       dies macht jedoch mit Keras Probleme, da hier bei RNNs die Batchsize nicht unterschiedlich
        # #       sein darf !!!!
        # if imgs.shape[0] < (self.batch_size * self.sequence_size):
            # reshaped_imgs.append(np.array(imgs))
            # reshaped_pre_speeds.append(np.array([np.array([v]) for v in pre_speeds]))
            # reshaped_speeds.append(np.array([np.array([v]) for v in speeds]))
            # reshaped_speed_cost_weights.append(np.array([np.array([v]) for v in speed_cost_weights]))
        # else:
            # for i in range(self.batch_size):
                # reshaped_imgs.append(np.array(imgs[i*self.sequence_size:(i+1)*self.sequence_size]))
                
                # reshaped_pre_speeds.append(  np.array([v for v in pre_speeds[i*self.sequence_size   : (i+1)*self.sequence_size]]))
                # reshaped_speeds.append(      np.array([np.array([v]) for v in speeds[i*self.sequence_size       : (i+1)*self.sequence_size]]))
                # reshaped_speed_cost_weights.append(np.array([v for v in speed_cost_weights[i*self.sequence_size : (i+1)*self.sequence_size]]))
        
        # if self.return_weights:
            # return ([np.asarray(reshaped_imgs), np.asarray(reshaped_pre_speeds)], [np.asarray(reshaped_speeds)], [np.asarray(reshaped_speed_cost_weights)])
            
        # else:
            # return ([np.asarray(reshaped_imgs), np.asarray(reshaped_pre_speeds)], [np.asarray(reshaped_speeds)])


    # def _get_data_list(self):
        
        # data_list = []
        
        # for key in self.dic.keys():
        
            # img_name = key
            # if "img_name" in self.dic[key].keys():
                # img_name = self.dic[key]["img_name"]
        
            # path = self.main_dir
            # if "img_path" in self.dic[key].keys():
                # path = self.dic[key]["img_path"]
        
            # img_path = "{}/{}.{}".format(path, img_name, self.img_file_type)
            # speed = self.dic[key]["speed"]
            # cost_weight = self.dic[key]["speed_cost_weight"]
            # data_list.append((key, img_path, speed, cost_weight))
        
        # self.data_list = data_list