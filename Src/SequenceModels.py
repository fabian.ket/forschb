# ==============================================
# ==============================================
# ==============================================
#                 MODEL STUFF
# ==============================================
# ==============================================
# ==============================================

from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras.layers import InputLayer, Input, Dense, Flatten, Reshape, Dropout, Activation, Concatenate
from tensorflow.keras.layers import LSTM, GRU, TimeDistributed
from tensorflow.keras.layers import Conv2D, MaxPooling2D, BatchNormalization, LeakyReLU
from tensorflow.keras.regularizers import l2
from tensorflow.keras import backend as K
import tensorflow.keras as k

def generator_model5(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(64, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(48, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model

def generator_model6(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
    
    
def generator_model6_both(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs, steer_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs, steer_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs, steer_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs, steer_inputs], outputs=[speed_outputs, steer_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
    
    
def generator_model6_both_own_lstm(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    
    # SPEED PART START ...
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs, steer_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs, steer_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs, steer_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    speed_lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(speed_lstm01)
    # ... SPEED PART END
    
    
    # STEER PART START ...
    steer_conc01 = Concatenate(name="steer_conc01_gen")([flatten, speed_inputs, steer_inputs])

    steer_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="steer_dense01_gen")(steer_conc01)
    steer_dens_drop01 = TimeDistributed(Dropout(0.2), name="steer_dense_dropout01")(speed_dens01)
    steer_dens_norm01 = TimeDistributed(BatchNormalization(), name="steer_dense_batch_norm01")(steer_dens_drop01)
    steer_dens_tanh01 = TimeDistributed(Activation("tanh"), name="steer_dense_tanh01_gen")(steer_dens_norm01)
    
    
    steer_dens_tanh01.set_shape((batch_size, sequence_size, steer_dens_tanh01.shape[2]))
    steer_conc02 = Concatenate(name="steer_conc02_gen")([steer_dens_tanh01, speed_inputs, steer_inputs])
    
    steer_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="steer_dense02_gen")(steer_conc02)
    steer_dens_drop02 = TimeDistributed(Dropout(0.2), name="steer_dense_dropout02")(steer_dens02)
    steer_dens_norm02 = TimeDistributed(BatchNormalization(), name="steer_dense_batch_norm02")(steer_dens_drop02)
    steer_dens_tanh02 = TimeDistributed(Activation("tanh"), name="steer_dense_tanh02_gen")(steer_dens_norm02)
    
    
    steer_dens_tanh02.set_shape((batch_size, sequence_size, steer_dens_tanh02.shape[2]))
    steer_conc03 = Concatenate(name="steer_conc03_gen")([steer_dens_tanh02, speed_inputs, steer_inputs])
    
    steer_conc03.set_shape((batch_size, sequence_size, steer_conc03.shape[2]))
    steer_lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="steer_lstm01_gen")(steer_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs_gen")(steer_lstm01)
    # ... STEER PART END
    
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs, steer_inputs], outputs=[speed_outputs, steer_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
    
    
def generator_model6_both_in_speed_out(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs, steer_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs, steer_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs, steer_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs, steer_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
    
    
def generator_model6_both_in_steer_out(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs, steer_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs, steer_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs, steer_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs, steer_inputs], outputs=[steer_outputs], name="generator")

    if compile_model:
        model.compile(loss={"steer_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"steer_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
    
    
def generator_model7(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(512, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(512, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(256, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model