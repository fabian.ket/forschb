#!/usr/bin/env python

from abc import ABC, abstractmethod


class AbstractIterator(ABC):
    def __init__(self, gen):
        self._gen = gen
        
    @abstractmethod
    def __iter__(self):
        raise NotImplementedError("Method __iter__() not implemented in class {}".format(self.__class__.__name__))

    def get_iterator(self):
        return self.__iter__()
        
        
class SeqGenIterator(AbstractIterator):

    def __init__(self, gen):
        AbstractIterator.__init__(self, gen)
        
        self.return_img_input   = gen.return_img_input
        self.return_speed_input = gen.return_speed_input
        self.return_steer_input = gen.return_steer_input
        
        self.return_speed_output = gen.return_speed_output
        self.return_steer_output = gen.return_steer_output
        
        self.return_speed_weights = gen.return_speed_weights
        self.return_steer_weights = gen.return_steer_weights

    def __iter__(self):
        for [img_batch, pre_sp_batch, pre_st_batch], [_] in self._gen:
            
            for img_seq, pre_sp_seq, pre_st_seq in zip(img_batch, pre_sp_batch, pre_st_batch):
                
                for img, pre_sp, pre_st in zip(img_seq, pre_sp_seq, pre_st_seq):
                    
                    yield img, pre_sp, pre_st
                    # inputs  = []
                    # outputs = []
                    # weights = []
                    
                    # print(type(examples[0]))
                    # print(len(examples[0]))
                    
                    # # INPUTS
                    # if self.return_img_input:
                        # inputs.append(examples[0][0])
                        
                    # if self.return_speed_input:
                        # inputs.append(examples[0][1])
                        
                    # if self.return_steer_input:
                        # inputs.append(examples[0][-1])
                    
                    
                    # # OUTPUTS
                    # if self.return_speed_output:
                        # outputs.append(examples[1][0])
                        
                    # if self.return_steer_output:
                        # outputs.append(examples[1][-1])
                    
                    
                    # # WEIGHTS
                    # if self.return_speed_weights:
                        # weights.append(examples[2][0])
                        
                    # if self.return_steer_weights:
                        # weights.append(examples[2][-1])
                    
                    
                    # returns = []
                    # returns.append(inputs)
                    
                    # if self.return_speed_output or self.return_steer_output:
                        # returns.append(outputs)
                        
                    # if self.return_speed_weights or self.return_steer_weights:
                        # returns.append(weights)
                
                    # yield returns
                    
                    
                    
                    
                    
                    
class GenIterator(AbstractIterator):
    def __iter__(self):
        for [img_batch, pre_steer_batch], _, _ in self._gen:
            for img, pre_steer in zip(img_batch, pre_steer_batch):
                yield img, pre_steer
                
class BothSeqIterator(AbstractIterator):
    def __iter__(self):
        for [img_batch, pre_steer_batch, pre_speed_batch], _, _ in self._gen:
            for img_seq, pre_steer_seq, pre_speed_seq, in zip(img_batch, pre_steer_batch, pre_speed_batch):
                for img, pre_steer, pre_speed in zip(img_seq, pre_steer_seq, pre_speed_seq):
                    yield img, pre_steer, pre_speed
                    
class SimpleIterator(AbstractIterator):
    def __iter__(self):
        for img_batch, label_batch in self._gen:
            for img, label in zip(img_batch, label_batch):
                yield img, label