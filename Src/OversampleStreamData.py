import numpy as np
import matplotlib.pyplot as plt
import copy
import cv2
import os
import Src.YamlUtils as yu


def analyze_class_in_dic(dic, class_key="steering_class", verbose=1):
    n_classes = len(list(dic.values())[0][class_key])
    
    key_lists_results = [ [] for _ in range(n_classes) ]
    
    for k, v in dic.items():
        
        one_hot_class = v[class_key]
        key_lists_results[np.argmax(one_hot_class)].append(k)
    
    values = [len(l) for l in key_lists_results]
    
    if verbose > 0:
        print("==================================================")
        print("distribution in dict \n")
        
        for i, l in enumerate(key_lists_results):
            print(f"{i}: {len(l)}")

        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        xlabel = range(n_classes)
        ax.bar(xlabel, values)
        plt.show()
        
    return key_lists_results


def oversample_class_in_dic(dic, analyzed_key_list, class_key="steering_class", max_samples=None, factor=10, verbose=1):
    
    n_classes = len(list(dic.values())[0][class_key])
    
    values = [len(l) for l in analyzed_key_list]
    
    max_class = max(values) 
    if max_samples is not None:
        max_class = min([max_class, max_samples])
    
    oversampled_key_list = copy.deepcopy(analyzed_key_list)
    
    for l, v in zip(oversampled_key_list, values):
        
        tmp = list(l)
        
        if len(l) <= 0:
            continue
        
        rounds = (max_class // len(l)) - 1
        
        if (v * factor) < max_class:
            rounds = ( (v * factor) // len(l) ) - 1
        
        for _ in range(rounds):
            
            for e in tmp:
            
                l.append(e)
    
    if verbose > 0:
        print("==================================================")
        print("distribution after oversampling \n")
        
        for i, l in enumerate(oversampled_key_list):
            print(f"{i}: {len(l)}")

            
        values = [len(l) for l in oversampled_key_list]
        
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        xlabel = range(n_classes)
        ax.bar(xlabel, values)
        plt.show()
        
        
    i = 0
    result = {}
        
    for l in oversampled_key_list:
        
        for k in l:
            
            result[i] = dic[k]
            
            img_name = k
            
            if "img_name" in dic[k].keys():
                img_name = dic[k]["img_name"]
            
            result[i]["img_name"] = img_name
            
            i += 1
            
    return result
    
    
def oversample_class_in_dic_sorted(dic, sorted_key_list, class_key="steering_class", max_repeat=None, verbose=1):
    
    n_classes = len(list(dic.values())[0][class_key])
    
    values = [len(l) for l in sorted_key_list]
    
    max_n_steer_class = max(values) 
    
    oversampled_key_list = copy.deepcopy(sorted_key_list)
    
    for l in oversampled_key_list:
        
        tmp = list(l)
        
        if len(l) <= 0:
            continue
        
        
        n_rounds = (max_n_steer_class // len(l)) - 1
        
        if max_repeat is not None:
        
            max_rounds = max_repeat - 1
            
            if n_rounds > (max_rounds):
            
                n_rounds = max_rounds
        
        for _ in range( n_rounds ):
            
            for e in tmp:
            
                l.append(e)
    
    if verbose > 0:
        print("==================================================")
        print("distribution after oversampling \n")
        
        for i, l in enumerate(oversampled_key_list):
            print(f"{i}: {len(l)}")

            
        values = [len(l) for l in oversampled_key_list]
            
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        xlabel = range(n_classes)
        ax.bar(xlabel, values)
        plt.show()
        
    
    tmp = []
    
    for l in oversampled_key_list:
        tmp += l
    
    tmp.sort()
    
    i = 0
    result = {}
    
    for key in tmp:

        result[i] = dic[key]
        result[i]["img_name"] = key
        
        i += 1
            
    return result
    

def load_imgs_from_dic(dic, img_path, img_file_type, keys):
    imgs = []
    
    for key in keys:
        img_name = key
        if "img_name" in dic[key].keys():
            img_name = dic[key]["img_name"]
        
        file_name = "{}/{}.{}".format(img_path, img_name, img_file_type)

        img = cv2.imread(file_name)
        
        imgs.append(img)
        
    return imgs


def sort_key_list(keys):
    result = []
    
    tmp = []
    pre_key = None
    
    length = len(keys)
    
    if length == 1:
        result.append(keys[0])
        
    else:
        for i, key in enumerate(keys):

            if pre_key is not None:

                if (key - 1) == pre_key:
                    tmp.append(key)
                    pre_key = key

                    if i == (length - 1):
                        result.append(tmp)

                else:
                    result.append(tmp)

                    tmp = []
                    tmp.append(key)
                    pre_key = key

                    if i == (length - 1):
                        result.append(tmp)

            else:
                tmp.append(key)
                pre_key = key
            
    return result

def calc_number_of_stretched_stream_imgs(n_imgs, n_stretch_rounds):
    return (n_imgs - 1) * 2**(n_stretch_rounds) + 1

def calc_number_of_stretched_stream_imgs_based_on_sorted_keys(keys, n_stretch_rounds):
    result = 0
    
    for l in keys:
        result += calc_number_of_stretched_stream_imgs(len(l), n_stretch_rounds)
        
    return result

def calc_n_stretch_rounds_based_on_sorted_keys(n_max_imgs, keys):
    
    n_rounds = 5
    
    while True:
        n_imgs = calc_number_of_stretched_stream_imgs_based_on_sorted_keys(keys, n_rounds)
    
        if n_imgs < n_max_imgs:
            n_rounds += 1
            
            n_imgs_now = calc_number_of_stretched_stream_imgs_based_on_sorted_keys(keys, n_rounds)
            
            if n_imgs_now > n_max_imgs:
                # diff1 = abs(n_max_imgs - n_imgs)
                # diff2 = abs(n_max_imgs - n_imgs_now)
                
                # if diff1 <= diff2:
                n_rounds -= 1
                break
                # else:
                #     break
            
        elif n_imgs > n_max_imgs:
            n_rounds -= 1
            
            n_imgs_now = calc_number_of_stretched_stream_imgs_based_on_sorted_keys(keys, n_rounds)
            
            if n_imgs_now < n_max_imgs:
                # diff1 = abs(n_max_imgs - n_imgs)
                # diff2 = abs(n_max_imgs - n_imgs_now)
                
                # if diff1 < diff2:
                #     n_rounds += 1
                #     break
                # else:
                break
            
        else:
            break
    
    return n_rounds

def load_imgs_from_dic(dic, img_path, img_file_type, keys):
    imgs = []
    
    for key in keys:
        img_name = key
        if "img_name" in dic[key].keys():
            img_name = dic[key]["img_name"]
        
        path = img_path
        if "img_path" in dic[key].keys():
            path = dic[key]["img_path"]
        
        file_name = "{}/{}.{}".format(path, img_name, img_file_type)

        img = cv2.imread(file_name)
        
        imgs.append(img)
        
    return imgs

def stretch_data(data, stretch_rounds=3):
    
    result = copy.deepcopy(data)
    
    for _ in range(stretch_rounds):
        
        data_to_add = []
        
        for i in range( (len(result) - 1) ):
            
            mean_data = (result[i+1] + result[i]) / 2.0
            data_to_add.append((i+1, mean_data))
        
        
        idx_offset = 0
        
        for i, dat in data_to_add:
            result.insert(i + idx_offset, dat)
            idx_offset +=1
        
    return result

def get_values_from_dic(dic, keys, target_key):
    
    result = []
    
    for key in keys:
        value = dic[key][target_key]
        result.append(value)
        
    return result

def key_in_dic(dic, key, default_key):
    if key in dic.keys():
        return key
    else:
        return default_key

def create_dirs(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == os.errno.EEXIST and os.path.isdir(path):
            pass
        else:
            print("Could not create dir {}".format(path))
            raise

def num_after_point(x):
    s = str(x)
    if not '.' in s:
        return 0
    return len(s) - s.index('.') - 1
            
def oversamle_stream_class_data(dic, img_path, dest_path, class_key="steering_class", img_file_type="png", dest_img_file_type="png", verbose=0):
    
    key_lists = analyze_class_in_dic(dic, class_key=class_key, verbose=verbose)
    
    max_n_samples_in_class = max([len(l) for l in key_lists])
    
    result_dic = {}
    
    create_dirs(dest_path + "/imgs")
    
    for key_list in key_lists:
        
#         if len(key_list) == max_n_samples_in_class:
#             continue

        if len(key_list) <= 0:
            continue
        
        sorted_key_list = sort_key_list(key_list)
        
        n_stretch_rounds = calc_n_stretch_rounds_based_on_sorted_keys(max_n_samples_in_class, sorted_key_list)
        
        for keys in sorted_key_list:
            
            if len(key_list) != max_n_samples_in_class:
                imgs = load_imgs_from_dic(dic, img_path, img_file_type, keys)
            
                steer_key = key_in_dic(dic[keys[0]], "orig_steering", "steering")
                steers = get_values_from_dic(dic, keys, steer_key)
            
                speed_key = key_in_dic(dic[keys[0]], "orig_speed", "speed")
                speeds = get_values_from_dic(dic, keys, speed_key)
            
                # normalize
                imgs = [img / 255.0 for img in imgs]
            
                stretched_imgs = stretch_data(imgs, n_stretch_rounds)
                del imgs
            
                stretched_steers = stretch_data(steers, n_stretch_rounds)
                del steers
            
                stretched_speeds = stretch_data(speeds, n_stretch_rounds)
                del speeds
            
                new_keys = list(np.linspace(keys[0], keys[-1], len(stretched_imgs), dtype=float))
            
                # denormalize
                stretched_imgs = [np.uint8(stretched_img * 255.0) for stretched_img in stretched_imgs]
                
            else:
                stretched_imgs = load_imgs_from_dic(dic, img_path, img_file_type, keys)
                
                steer_key = key_in_dic(dic[keys[0]], "orig_steering", "steering")
                stretched_steers = get_values_from_dic(dic, keys, steer_key)
                
                speed_key = key_in_dic(dic[keys[0]], "orig_speed", "speed")
                stretched_speeds = get_values_from_dic(dic, keys, speed_key)
                
                new_keys = keys
            
            # show_imgs(stretched_imgs)
            for new_key, stretched_img in zip(new_keys, stretched_imgs):
                cv2.imwrite(dest_path + "/imgs/{}.{}".format(float(new_key), dest_img_file_type), stretched_img)
            
            del stretched_imgs
            
            for key, steer, speed in zip(new_keys, stretched_steers, stretched_speeds):
                
                key = float(key)
                
                result_dic[key] = {}
                result_dic[key]["steering"] = float(steer)
                result_dic[key]["speed"] = float(speed)
            
    yu.write_dict_to_yaml(dest_path + "/training.yaml", result_dic)