import numpy as np
import Src.Speed as Speed

def get_steer_values_dic():
    return {0: -0.9 , 1: -0.75, 2: -0.6 ,  3: -0.45,  4: -0.3 ,  5: -0.15, 
            6:  0.0, 
            7:  0.15, 8:  0.3 , 9:  0.45, 10:  0.6 , 11:  0.75, 12:  0.9}
    
    
def get_speed_values_dic():
    return {0: 0.05 , 1: 0.125, 2: 0.2 ,  3: 0.275,  4: 0.35 ,  5: 0.425, 
            6:  0.5, 
            7:  0.575, 8:  0.65 , 9:  0.725, 10:  0.8 , 11:  0.875, 12:  0.95}
    
    
def steer_class_to_value(steer_class):
    values = get_steer_values_dic()
    return values[np.argmax(steer_class)]

def get_max_abs_speed_and_steering(dic):

    max_speed = -1
    max_steer = -1

    for key in dic:
    
        speed = abs(dic[key]["speed"])
        steer = abs(dic[key]["steering"])
    
        max_speed = np.maximum(speed, max_speed)
        max_steer = np.maximum(steer, max_steer)
            
    return max_speed, max_steer

    
def add_speed_classes_to_dic(dic, threshold=0.1):
    
    speed_before = 0.0
    
    for key in dic:
        
        speed = dic[key]["speed"]
        
        one_hot = np.zeros(Speed.Speed.len())
        
        if (speed_before + threshold) > speed and (speed_before - threshold) < speed:
            one_hot[Speed.Speed.MAINTAIN] = 1
            
        elif speed_before + threshold <= speed:
            one_hot[Speed.Speed.ACCELERATE] = 1
            speed_before = speed
            
        elif speed_before - threshold >= speed:
            one_hot[Speed.Speed.DEACCELERATE] = 1
            speed_before = speed
            
        dic[key]["speed_class"] = one_hot

def add_speed_classes_to_dic_like_steering(dic):
    
    values = get_speed_values_dic()
    
    threshold = np.round(np.abs(values[0] - values[1]), 4)
    half = threshold / 2.0
    
    smallest =  1.0
    highest  = -1.0
    
    for k in values:
        
        v = values[k]
        
        smallest = np.minimum(smallest, v)
        highest  = np.maximum(highest,  v)
    
    n_classes = len(values)
    
    for k in dic.keys():
        speed = dic[k]["speed"]
        
        for k2 in values.keys():
            
            v = values[k2]
            
            left_border = 0.0
            right_border = 0.0
            
            if v == smallest:
                left_border = 10
                
            elif v == highest:
                right_border = 10
            
            if v - (half + left_border) <= speed and v + (half + right_border) >= speed:
                one_hot = np.zeros(n_classes)
                one_hot[k2] = 1.0
                dic[k]["speed_class"] = one_hot
                break
    
def add_steer_classes_to_dic(dic):
    
    values = get_steer_values_dic()
    
    threshold = np.round(np.abs(values[0] - values[1]), 4)
    half = threshold / 2.0
    
    smallest =  1.0
    highest  = -1.0
    
    for k in values:
        
        v = values[k]
        
        smallest = np.minimum(smallest, v)
        highest  = np.maximum(highest,  v)
    
    n_classes = len(values)
    
    for k in dic:
        steer = dic[k]["steering"]
        
        for k2 in values:
            
            v = values[k2]
            
            left_border = 0.0
            right_border = 0.0
            
            if v == smallest:
                left_border = 10
                
            elif v == highest:
                right_border = 10
            
            if v - (half + left_border) <= steer and v + (half + right_border) >= steer:
                one_hot = np.zeros(n_classes)
                one_hot[k2] = 1.0
                dic[k]["steering_class"] = one_hot
                break
    
    
def normalize_dic_steering(dic, max_abs_steer):
    for key in dic:
        orig_steering = dic[key]["steering"]
        norm_steering = orig_steering / max_abs_steer
        
        dic[key]["steering"] = norm_steering
        
        if "orig_steering" not in dic[key].keys():
            dic[key]["orig_steering"] = orig_steering
        
        
def normalize_dic_speed(dic, max_abs_speed):
    for key in dic:
        orig_speed = dic[key]["speed"]
        norm_speed = orig_speed / max_abs_speed
    
        dic[key]["speed"] = norm_speed
        
        if "orig_speed" not in dic[key].keys():
            dic[key]["orig_speed"] = orig_speed
        
        
def remove_data_with_speed_lower_than_threshold(dic, threshold=0.5):
    toRemove = []
    
    for key in dic:
        if dic[key]["speed"] < threshold:
            toRemove.append(key)
            
    for key in toRemove:
        del dic[key]
        
        
def add_speed_cost_weight_to_dic(dic):
    values = {}
    
    for key in dic.keys():
        speed_class = np.argmax(dic[key]["speed_class"])
        
        t = values.get(speed_class, 0.0)
        
        t += 1.0
        
        values[speed_class] = t
        
    length = len(dic)
    nClasses = len(values.keys())
    class_distribution = (1.0 / nClasses)
    
    for key in values:
        values[key] = class_distribution / (values[key] / length)
    
    result = np.zeros(length)
    
    for key in dic:
        speed_class = np.argmax(dic[key]["speed_class"])
        
        dic[key]["speed_cost_weight"] = values[speed_class]
        

def add_steer_cost_weight_to_dic_classifier(dic):
    values = {}
    
    for key in dic.keys():
        steering_class = np.argmax(dic[key]["steering_class"])
        
        t = values.get(steering_class, 0.0)
        
        t += 1.0
        
        values[steering_class] = t
        
    length = len(dic)
    nClasses = len(values.keys())
    class_distribution = (1.0 / nClasses)
    
    for key in values:
        values[key] = class_distribution / (values[key] / length)
    
    result = np.zeros(length)
    
    for key in dic:
        steering_class = np.argmax(dic[key]["steering_class"])
        
        dic[key]["steer_cost_weight"] = values[steering_class]
        
        
def add_steer_cost_weight_to_dic(dic, threshold=0.01):
    
    lefts = 0
    rights = 0
    middles = 0
    
    for key in dic.keys():
        steering = dic[key]["steering"]
        
        if steering <= -threshold:
            lefts += 1
            
        elif steering >= threshold:
            rights += 1
            
        else:
            middles += 1
            
    length = len(dic)
    nClasses = 3.0
    class_distribution = (1.0 / nClasses)
    
    result = np.zeros(length)
    
    left_weight = class_distribution / (lefts / length) if lefts > 0 else 0
    right_weight = class_distribution / (rights / length) if rights > 0 else 0
    middle_weight = class_distribution / (middles / length) if middles > 0 else 0
    
    for key in dic:
        
        steering = dic[key]["steering"]
        
        if steering <= -threshold:
            dic[key]["steer_cost_weight"] = left_weight
            
        elif steering >= threshold:
            dic[key]["steer_cost_weight"] = right_weight
            
        else:
            dic[key]["steer_cost_weight"] = middle_weight


import Src.YamlUtils as yu

def convert_neuro_data_to_yaml(file):
    result = {}
    
    yaml = yu.read_yaml(file)
    
    for entry in yaml:
        
        img = entry["camera_messages"][0].split("/")[-1].split(".")[0]
        
        steering = entry["action_messages"]["/engine_wrapper/output/default"]["drive"]["steering_angle"]
        speed = entry["action_messages"]["/engine_wrapper/output/default"]["drive"]["speed"]
        
        result[img] = {"speed": speed, "steering": steering}
        
    return result
    

import cv2
import copy

def convert_nero_keys_to_numbers(dic, img_path):
    key_n = 0
    
    result_dic = {}
    
    for key in dic.keys():
    
        current = dic[key]
    
        result_dic[key_n] = copy.deepcopy(current)
        
        current_result = result_dic[key_n]
        
        dic_keys = current.keys()
        
        if "img_name" not in dic_keys:
            current_result["img_name"] = key
            
        if "img_path" not in dic_keys:
            current_result["img_path"] = img_path
            
        key_n += 1
        
    return result_dic
        
        