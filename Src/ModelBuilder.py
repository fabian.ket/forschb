#!/usr/bin/env python

from abc import ABC, abstractmethod

import Src.GenericModel as gm

class ModelBuilder(ABC):
    
    @abstractmethod
    def build(self):
        raise NotImplementedError
    
    
class SeqModelBuilder(ModelBuilder):
    def __init__(self, f_model, 
                 rnn_layername,
                 img_shape, 
                 batch_size=1, 
                 stateful=True, 
                 compile_model=True, 
                 optimizer="adam", 
                 bias=True,
                 weights_file=None):
                 
        self.f_model       = f_model
        self.rnn_layername = rnn_layername
        
        self.img_shape     = img_shape
        self.batch_size    = batch_size
        self.stateful      = stateful
        self.compile_model = compile_model
        self.optimizer     = optimizer
        self.bias          = bias
        
        self.weights_file  = weights_file

    def build(self):
        model = self.f_model(image_shape=self.img_shape, 
                             batch_size=self.batch_size, 
                             stateful=self.stateful, 
                             compile_model=self.compile_model, 
                             optimizer=self.optimizer, 
                             bias=self.bias)
        
        if self.weights_file is not None:
            model.load_weights(self.weights_file)
        
        return gm.SeqModel(model, self.batch_size, self.img_shape, self.rnn_layername)