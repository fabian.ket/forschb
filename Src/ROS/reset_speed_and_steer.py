#!/usr/bin/env python

import rospy
import time

from geometry_msgs.msg import TwistStamped

def main():
    rospy.init_node('ai_two_models', anonymous=True)
    pub = rospy.Publisher('/nr/ai/output/actions', TwistStamped, queue_size=10)
    
    twist = TwistStamped()
    twist.twist.linear.x  = 0.0 # speed
    twist.twist.angular.z = 0.0 # steering
    
    for _ in range(3):
        pub.publish(twist)
        time.sleep(0.1)

if __name__ == "__main__":
    main()
