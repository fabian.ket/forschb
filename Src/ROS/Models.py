from keras.models import Model, Sequential, load_model
from keras.layers import InputLayer, Input, Dense, Flatten, Reshape, Dropout, Activation, Concatenate
from keras.layers import LSTM, CuDNNLSTM, GRU, CuDNNGRU, TimeDistributed
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.regularizers import l2

from keras import backend as K
import keras as k
import tensorflow as tf


def reset_session():
    K.clear_session()
    tf_config = tf.compat.v1.ConfigProto()
    tf_config.gpu_options.allow_growth = True
    K.tensorflow_backend.set_session(tf.compat.v1.Session(config=tf_config))
    
    
# # https://machinelearningmastery.com/cnn-long-short-term-memory-networks/

def gru(input_shape, verbose=True):
    
    batch_size = input_shape[0]
    sequence_size = None
    img_shape = input_shape[2:]
    
    model = Sequential()
    # model.add(InputLayer(batch_input_shape=(batch_size, sequence_size, img_shape[0], img_shape[1], img_shape[2]), name="input"))
    
    model.add(TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=False,
                                     batch_size=batch_size, 
                                     input_shape=(sequence_size, img_shape[0], img_shape[1], img_shape[2])
                              ),
                              name="conv01"))
    
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm01"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu01"))

    model.add(TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=False), name="conv02"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm02"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu02"))

    model.add(TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False), name="conv03"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm03"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu03"))

    model.add(TimeDistributed(Conv2D(filters= 32, kernel_size=(3, 3), padding="same", use_bias=False), name="conv04"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm04"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu04"))

    model.add(TimeDistributed(Flatten(), name="flatten"))

    model.add(TimeDistributed(Dense(32, use_bias=False), name="dense01"))
    model.add(TimeDistributed(BatchNormalization(), name="dense_batch_norm01"))
    model.add(TimeDistributed(Activation("tanh"), name="dense_tanh01"))
    model.add(TimeDistributed(Dropout(0.5), name="dense_dropout01"))

    model.add(GRU(32, return_sequences=True, stateful=True, name="gru01"))

    model.add(TimeDistributed(Dense(1, activation='tanh'), name="output"))
    # ARCHITECTURE END
    
    # MODEL BEGIN
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    
    if verbose: 
        print(model.summary())
    # MODEL END
    
    print("RETRUN GRU")
    
    return model

def lstm(input_shape, verbose=True):

    # ARCHITECTURE BEGIN LSTM02.5
    model = Sequential()
    model.add(TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=False), name="conv01", input_shape=input_shape))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm01"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu01"))

    model.add(TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=False), name="conv02"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm02"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu02"))

    model.add(TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False), name="conv03"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm03"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu03"))

    model.add(TimeDistributed(Conv2D(filters= 32, kernel_size=(3, 3), padding="same", use_bias=False), name="conv04"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm04"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu04"))

    model.add(TimeDistributed(Flatten(), name="flatten"))

    model.add(TimeDistributed(Dense(32, use_bias=False), name="dense01"))
    model.add(TimeDistributed(BatchNormalization(), name="dense_batch_norm01"))
    model.add(TimeDistributed(Activation("tanh"), name="dense_tanh01"))
    model.add(TimeDistributed(Dropout(0.5), name="dense_dropout01"))

    model.add(LSTM(32, return_sequences=True, name="lstm01"))

    model.add(TimeDistributed(Dense(1, activation='tanh'), name="output"))
    # ARCHITECTURE END
    
    # MODEL BEGIN
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    
    if verbose: 
        print(model.summary())
    # MODEL END
    
    return model

def lstm_5mio(input_shape, verbose=True):

    # ARCHITECTURE BEGIN LSTM02.5
    model = Sequential()
    model.add(TimeDistributed(Conv2D(filters=128, kernel_size=(11, 11), padding="same", use_bias=False), name="conv01", input_shape=input_shape))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm01"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu01"))

    model.add(TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=False), name="conv02"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm02"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu02"))

    model.add(TimeDistributed(Conv2D(filters=512, kernel_size=(3, 3), padding="same", use_bias=False), name="conv03"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm03"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu03"))

    model.add(TimeDistributed(Conv2D(filters= 256, kernel_size=(3, 3), padding="same", use_bias=False), name="conv04"))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04"))
    model.add(TimeDistributed(BatchNormalization(), name="conv_batch_norm04"))
    model.add(TimeDistributed(Activation("relu"), name="conv_relu04"))

    model.add(TimeDistributed(Flatten(), name="flatten"))

    model.add(TimeDistributed(Dense(256, use_bias=False), name="dense01"))
    model.add(TimeDistributed(BatchNormalization(), name="dense_batch_norm01"))
    model.add(TimeDistributed(Activation("tanh"), name="dense_tanh01"))
    model.add(TimeDistributed(Dropout(0.5), name="dense_dropout01"))

    model.add(LSTM(256, return_sequences=True, name="lstm01"))

    model.add(TimeDistributed(Dense(1, activation='tanh'), name="output"))
    # ARCHITECTURE END
    
    # MODEL BEGIN
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    
    if verbose: 
        print(model.summary())
    # MODEL END
    
    return model

def cnn(input_shape, verbose=True):
    
    # ARCHITECTURE BEGIN CNN
    model = Sequential()
    model.add(Conv2D(filters= 32, kernel_size=(11, 11), padding="same", use_bias=False, name="conv01", input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2, 2), name="pool01"))
    model.add(BatchNormalization(name="batch_norm01"))
    model.add(Activation("relu", name="relu01"))

    model.add(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=False, name="conv02"))
    model.add(MaxPooling2D(pool_size=(2, 2), name="pool02"))
    model.add(BatchNormalization(name="batch_norm02"))
    model.add(Activation("relu", name="relu02"))

    model.add(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False, name="conv03"))
    model.add(MaxPooling2D(pool_size=(2, 2), name="pool03"))
    model.add(BatchNormalization(name="batch_norm03"))
    model.add(Activation("relu", name="relu03"))

    model.add(Conv2D(filters= 32, kernel_size=(3, 3), padding="same", use_bias=False, name="conv04"))
    model.add(MaxPooling2D(pool_size=(2, 2), name="pool04"))
    model.add(BatchNormalization(name="batch_norm04"))
    model.add(Activation("relu", name="relu04"))

    model.add(Flatten(name="flatten"))

    model.add(Dense(32, use_bias=False, name="dense01"))
    model.add(BatchNormalization(name="dense_batch_norm01"))
    model.add(Activation("tanh", name="dense_tanh01"))
    model.add(Dropout(0.5, name="dense_dropout01"))

    model.add(Dense(32, use_bias=False, name="dense02"))
    model.add(BatchNormalization(name="dense_batch_norm02"))
    model.add(Activation("tanh", name="dense_tanh02"))

    model.add(Dense(1, activation='tanh', name="output"))
    # ARCHITECTURE END

    # MODEL BEGIN
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    
    if verbose: 
        print(model.summary())
    # MODEL END
    
    return model


def small_net(image_shape, verbose=True):
    """
    Self driving approach with a small network

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    model = Sequential()

    model.add(Conv2D(
        filters=8,
        kernel_size=(7, 7),
        activation="relu",
        name="conv1",
        kernel_initializer="glorot_uniform",
        input_shape=image_shape,
        strides=2,
        kernel_regularizer=k.regularizers.l1(0.008)))

    model.add(BatchNormalization())

    model.add(Conv2D(
        filters=16,
        kernel_size=(5, 5),
        activation="relu",
        name="conv2",
        kernel_initializer="glorot_uniform",
        kernel_regularizer=k.regularizers.l1(0.008)))

    model.add(BatchNormalization())

    model.add(Conv2D(
        filters=16,
        kernel_size=(5, 5),
        activation="relu",
        name="conv3",
        kernel_initializer="glorot_uniform",
        kernel_regularizer=k.regularizers.l1(0.002)))

    model.add(Flatten(name="flattened_observation"))

    model.add(Dense(
        units=150, activation="relu", kernel_initializer="glorot_uniform"))

    model.add(Dropout(0.2))

    model.add(Dense(
        units=80, activation="relu", kernel_initializer="glorot_uniform"))

    model.add(Dense(
        units=1, kernel_initializer="glorot_uniform", name="prediction"))

    model.compile(loss="mse", optimizer="adam", metrics=["mae"])
    
    if verbose: 
        print(model.summary())

    return model

def nvidia(image_shape, verbose=True):
    """
    The architecture which has been used in the End to End Learning for
    Self-Driving Cars paper (https://arxiv.org/abs/1604.07316) from nvidia.

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    model = Sequential()

    # Convolution no.1
    model.add(
        Conv2D(filters=24, kernel_size=(5, 5), strides=(2, 2),
               bias_initializer="he_normal", padding="valid",
               input_shape=image_shape, activation="relu"))

    # Convolution no.2
    model.add(
        Conv2D(filters=36, kernel_size=(5, 5), strides=(2, 2),
               bias_initializer="he_normal", padding="valid",
               activation="relu"))

    # Convolution no.3
    model.add(
        Conv2D(filters=48, kernel_size=(5, 5), strides=(1, 1), # stride wurde angepasst auf (1,1) von (2,2) wegen input_shape
               bias_initializer="he_normal", padding="valid",
               activation="relu"))

    # Convolution no.4
    model.add(
        Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
               padding="valid", activation="relu"))

    # Convolution no.5
    model.add(
        Conv2D(filters=64, kernel_size=(3, 3), bias_initializer="he_normal",
               padding="valid", activation="relu"))
    model.add(Flatten())

    # Fully connected no.1
    model.add(Dense(units=1164, bias_initializer="he_normal", activation="relu"))
    model.add(Dropout(0.2))

    # Fully connected no.2
    model.add(Dense(units=100, bias_initializer="he_normal", activation="relu"))
    model.add(Dropout(0.2))

    # Fully connected no.3
    model.add(Dense(units=50, bias_initializer="he_normal", activation="relu"))

    # Fully connected no.4
    model.add(Dense(units=10, bias_initializer="he_normal", activation="tanh"))

    # Fully connected no.5
    model.add(Dense(units=1, bias_initializer="he_normal", activation="tanh",
                    name="prediction"))

    model.compile(loss="mse", optimizer="adam", metrics=["mae"])
    
    if verbose: 
        print(model.summary())

    return model
    
    
def fabian_ket_gru_research_a(image_shape, batch_size=1):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (1, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
    
    speed_inputs = Input(batch_shape=(batch_size, sequence_size, 1), name="speed_input")
    
    
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=False), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=False), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=False), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)

    steer_gru01, steer_gru01_h = GRU(32, return_sequences=True, return_state=True, stateful=False, 
                                     name="steer_gru01")(dens_drop01)
    
    # speed_conc = Concatenate()([dens_drop01, speed_inputs])
    
    speed_gru01, speed_gru01_h = GRU(32, return_sequences=True, return_state=True, stateful=False, 
                                     name="speed_gru01")(speed_inputs)

    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs")(steer_gru01)
    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs")(speed_gru01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[steer_outputs, speed_outputs])
    model.compile(loss='mse', optimizer='adam', metrics=['mse', 'mae'])

    return model
    

def fabian_ket_lstm_research_b_speed(image_shape, batch_size=1, stateful=True, compile_model=True):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (batch_size, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    bias =  True
    
    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
                       
    speed_inputs = Input(batch_shape=(batch_size, sequence_size, 1), name="speed_input")
    
    conv01 = TimeDistributed(Conv2D(filters=128, kernel_size=(11, 11), padding="same", use_bias=bias), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(7, 7), padding="same", use_bias=bias), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=bias), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    # densSteer = TimeDistributed(Dense(32, use_bias=bias), name="denseSteer")(steer_inputs)
    # densSteer_norm01 = TimeDistributed(BatchNormalization(), name="denseSteer_batch_norm01")(densSteer)
    # densSteer_tanh01 = TimeDistributed(Activation("tanh"), name="denseSteer_tanh01")(densSteer_norm01)
    # densSteer_drop01 = TimeDistributed(Dropout(0.5), name="denseSteer_dropout01")(densSteer_tanh01)

    
    # concat = Concatenate(name="concat")([dens_drop01, densSteer_drop01])
    
    speed_lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="speed_lstm01")(dens_drop01)

    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs")(speed_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])
    
    if compile_model:
        model.compile(loss='mse', optimizer='adam', metrics=['mae'], sample_weight_mode="temporal")

    return model
    
    
def fabian_ket_lstm_research_b_steer(image_shape, batch_size=1, stateful=True, compile_model=True):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (batch_size, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    bias =  True
    
    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
                       
    steer_inputs = Input(batch_shape=(batch_size, sequence_size, 1), name="steer_input")
    
    conv01 = TimeDistributed(Conv2D(filters=128, kernel_size=(11, 11), padding="same", use_bias=bias), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(7, 7), padding="same", use_bias=bias), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=bias), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    # densSteer = TimeDistributed(Dense(32, use_bias=bias), name="denseSteer")(steer_inputs)
    # densSteer_norm01 = TimeDistributed(BatchNormalization(), name="denseSteer_batch_norm01")(densSteer)
    # densSteer_tanh01 = TimeDistributed(Activation("tanh"), name="denseSteer_tanh01")(densSteer_norm01)
    # densSteer_drop01 = TimeDistributed(Dropout(0.5), name="denseSteer_dropout01")(densSteer_tanh01)

    
    # concat = Concatenate(name="concat")([dens_drop01, densSteer_drop01])
    
    steer_lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="steer_lstm01")(dens_drop01)

    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs")(steer_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs], outputs=[steer_outputs])
    
    if compile_model:
        model.compile(loss='mse', optimizer='sgd', metrics=['mae'], sample_weight_mode="temporal")

    return model
    

def fabian_ket_lstm_research_b_steer2(image_shape, batch_size=1, stateful=True, compile_model=True):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (batch_size, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    bias =  True
    
    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
                       
    steer_inputs = Input(batch_shape=(batch_size, sequence_size, 1), name="steer_input")
    
    conv01 = TimeDistributed(Conv2D(filters=160, kernel_size=(15, 15), padding="same", use_bias=bias), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=128, kernel_size=(11, 11), padding="same", use_bias=bias), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=96, kernel_size=(7, 7), padding="same", use_bias=bias), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)
    
    conv05 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv05")(relu04)
    pool05 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool05")(conv05)
    norm05 = TimeDistributed(BatchNormalization(), name="conv_batch_norm05")(pool05)
    relu05 = TimeDistributed(Activation("relu"), name="conv_relu05")(norm05)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu05)

    dens01 = TimeDistributed(Dense(32, use_bias=bias), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    dens02 = TimeDistributed(Dense(32, use_bias=bias), name="dense02")(dens_drop01)
    dens_norm02 = TimeDistributed(BatchNormalization(), name="dense_batch_norm02")(dens02)
    dens_tanh02 = TimeDistributed(Activation("tanh"), name="dense_tanh02")(dens_norm02)
    dens_drop02 = TimeDistributed(Dropout(0.5), name="dense_dropout02")(dens_tanh02)
    
    
    steer_lstm01 = LSTM(32, return_sequences=True, return_state=False, 
                        stateful=stateful, name="steer_lstm01")(dens_drop02)

    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs")(steer_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs], outputs=[steer_outputs])
    
    if compile_model:
        model.compile(loss='mse', optimizer='sgd', metrics=['mae'], sample_weight_mode="temporal")

    return model
    
    
def fabian_ket_lstm_research_b_steer3(image_shape, batch_size=1, stateful=True, compile_model=True):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (batch_size, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    bias =  True
    
    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
                       
    steer_inputs = Input(batch_shape=(batch_size, sequence_size, 1), name="steer_input")
    
    conv01 = TimeDistributed(Conv2D(filters=256, kernel_size=(15, 15), padding="same", use_bias=bias), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=192, kernel_size=(11, 11), padding="same", use_bias=bias), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(7, 7), padding="same", use_bias=bias), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=96, kernel_size=(5, 5), padding="same", use_bias=bias), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)
    
    conv05 = TimeDistributed(Conv2D(filters=64, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv05")(relu04)
    pool05 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool05")(conv05)
    norm05 = TimeDistributed(BatchNormalization(), name="conv_batch_norm05")(pool05)
    relu05 = TimeDistributed(Activation("relu"), name="conv_relu05")(norm05)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu05)

    dens01 = TimeDistributed(Dense(256, use_bias=bias), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    dens02 = TimeDistributed(Dense(128, use_bias=bias), name="dense02")(dens_drop01)
    dens_norm02 = TimeDistributed(BatchNormalization(), name="dense_batch_norm02")(dens02)
    dens_tanh02 = TimeDistributed(Activation("tanh"), name="dense_tanh02")(dens_norm02)
    dens_drop02 = TimeDistributed(Dropout(0.5), name="dense_dropout02")(dens_tanh02)
    
    
    steer_lstm01 = CuDNNLSTM(64, return_sequences=True, return_state=False, 
                             stateful=stateful, name="steer_lstm01")(dens_drop02)

    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs")(steer_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs], outputs=[steer_outputs])
    
    if compile_model:
        model.compile(loss='mse', optimizer='sgd', metrics=['mae'], sample_weight_mode="temporal")

    return model
    
    
def fabian_ket_cnn_research_b_steer(image_shape, batch_size=1):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (1, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
    
    conv01 = Conv2D(filters=128, kernel_size=(11, 11), padding="same", use_bias=False, 
                             name="conv01")(img_inputs)
    pool01 = MaxPooling2D(pool_size=(2, 2), name="pool01")(conv01)
    norm01 = BatchNormalization(name="conv_batch_norm01")(pool01)
    relu01 = Activation("relu", name="conv_relu01")(norm01)

    conv02 = Conv2D(filters=96, kernel_size=(5, 5), padding="same", use_bias=False, 
                             name="conv02")(relu01)
    pool02 = MaxPooling2D(pool_size=(2, 2), name="pool02")(conv02)
    norm02 = BatchNormalization(name="conv_batch_norm02")(pool02)
    relu02 = Activation("relu", name="conv_relu02")(norm02)

    conv03 = Conv2D(filters=64, kernel_size=(3, 3), padding="same", use_bias=False, 
                             name="conv03")(relu02)
    pool03 = MaxPooling2D(pool_size=(2, 2), name="pool03")(conv03)
    norm03 = BatchNormalization(name="conv_batch_norm03")(pool03)
    relu03 = Activation("relu", name="conv_relu03")(norm03)

    conv04 = Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=False, 
                             name="conv04")(relu03)
    pool04 = MaxPooling2D(pool_size=(2, 2), name="pool04")(conv04)
    norm04 = BatchNormalization(name="conv_batch_norm04")(pool04)
    relu04 = Activation("relu", name="conv_relu04")(norm04)

    flatten = Flatten(name="flatten")(relu04)

    dens01 = Dense(32, use_bias=False, name="dense01")(flatten)
    dens_norm01 = BatchNormalization(name="dense_batch_norm01")(dens01)
    dens_tanh01 = Activation("tanh", name="dense_tanh01")(dens_norm01)
    dens_drop01 = Dropout(0.5, name="dense_dropout01")(dens_tanh01)
    
    dens02 = Dense(32, use_bias=False, name="dense02")(dens_drop01)
    dens_norm02 = BatchNormalization(name="dense_batch_norm02")(dens02)
    dens_tanh02 = Activation("tanh", name="dense_tanh02")(dens_norm02)
    dens_drop02 = Dropout(0.5, name="dense_dropout02")(dens_tanh02)

    steer_outputs = Dense(1, activation='tanh', name="steer_outputs")(dens_drop02)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs], outputs=[steer_outputs])
    model.compile(loss='mse', optimizer='sgd', metrics=['mae'])

    return model
    


    
def fabian_ket_lstm_research_b_steer_classifier(image_shape, batch_size=1, stateful=True):

    input_shape = (1, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    bias = False
    
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
    
    steer_inputs = Input(batch_shape=(batch_size, sequence_size, 13), name="steer_input")
    
    
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=bias), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=bias), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    # densSteer = TimeDistributed(Dense(32, use_bias=bias), name="denseSteer")(steer_inputs)
    # densSteer_norm01 = TimeDistributed(BatchNormalization(), name="denseSteer_batch_norm01")(densSteer)
    # densSteer_tanh01 = TimeDistributed(Activation("tanh"), name="denseSteer_tanh01")(densSteer_norm01)
    # densSteer_drop01 = TimeDistributed(Dropout(0.5), name="denseSteer_dropout01")(densSteer_tanh01)
    
    # concat = Concatenate(name="concat")([dens_drop01, densSteer_drop01])
    
    steer_lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="steer_lstm01")(dens_drop01)
                                     
    steer_outputs = TimeDistributed(Dense(13, activation='sigmoid'), name="steer_outputs")(steer_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs], outputs=[steer_outputs])
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'], sample_weight_mode="temporal")

    return model
    
    
def fabian_ket_lstm_research_b_steer_classifier2(image_shape, batch_size=1, stateful=True):

    input_shape = (1, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
    
    steer_inputs = Input(batch_shape=(batch_size, sequence_size, 13), name="steer_input")
    
    
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=False), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=False), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=False), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    densSteer = TimeDistributed(Dense(32, use_bias=False), name="denseSteer")(steer_inputs)
    densSteer_norm01 = TimeDistributed(BatchNormalization(), name="denseSteer_batch_norm01")(densSteer)
    densSteer_tanh01 = TimeDistributed(Activation("tanh"), name="denseSteer_tanh01")(densSteer_norm01)
    densSteer_drop01 = TimeDistributed(Dropout(0.5), name="denseSteer_dropout01")(densSteer_tanh01)
    
    concat = Concatenate(name="concat")([dens_drop01, densSteer_drop01])
    
    steer_lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="steer_lstm01")(concat)
                                     
    # speed_gru1 = TimeDistributed(Dense(32, use_bias=False), name="bla")(concat)
    # tanh01 = TimeDistributed(Activation("tanh"), name="denseSpeed_tanh01")(norm01)

    steer_outputs = TimeDistributed(Dense(13, activation='sigmoid'), name="steer_outputs")(steer_lstm01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs], outputs=[steer_outputs])
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'], sample_weight_mode="temporal")

    return model
    
    
def fabian_ket_gru_research_b_speed(image_shape, batch_size=1):
    """
    Self driving approach with a gru, research project a fabian ket
    net based on https://arxiv.org/abs/1801.06734

    :param image_shape: TODO
    :type image_shape: Tuple[int, int, int]
    :return: the constructed model instance
    :rtype: keras.models.Model
    """
    
    input_shape = (1, None, image_shape[0], image_shape[1], image_shape[2])
    sequence_size = None

    # ARCHITECTURE BEGIN GRUv2.5
    img_inputs = Input(batch_shape=(batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2]), 
                       name="img_input")
    
    speed_inputs = Input(batch_shape=(batch_size, sequence_size, 3), name="speed_input")
    
    
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=False), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=False), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=False), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)

    dens01 = TimeDistributed(Dense(32, use_bias=False), name="dense01")(flatten)
    dens_norm01 = TimeDistributed(BatchNormalization(), name="dense_batch_norm01")(dens01)
    dens_tanh01 = TimeDistributed(Activation("tanh"), name="dense_tanh01")(dens_norm01)
    dens_drop01 = TimeDistributed(Dropout(0.5), name="dense_dropout01")(dens_tanh01)
    
    densSpeed = TimeDistributed(Dense(32, use_bias=False), name="denseSpeed")(speed_inputs)
    densSpeed_norm01 = TimeDistributed(BatchNormalization(), name="denseSpeed_batch_norm01")(densSpeed)
    densSpeed_tanh01 = TimeDistributed(Activation("tanh"), name="denseSpeed_tanh01")(densSpeed_norm01)
    densSpeed_drop01 = TimeDistributed(Dropout(0.5), name="denseSpeed_dropout01")(densSpeed_tanh01)
    
    concat = Concatenate(name="concat")([dens_drop01, densSpeed_drop01])
    
    speed_gru01, speed_gru01_h = GRU(32, return_sequences=True, return_state=True, stateful=True, 
                                     name="speed_gru01")(concat)
                                     
    # speed_gru1 = TimeDistributed(Dense(32, use_bias=False), name="bla")(concat)
    # tanh01 = TimeDistributed(Activation("tanh"), name="denseSpeed_tanh01")(norm01)

    speed_outputs = TimeDistributed(Dense(3, activation='sigmoid'), name="speed_outputs")(speed_gru01)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'], sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_paper_both_reg(image_shape, batch_size=1, stateful=True,
                                                    compile_model=True, optimizer="adam", bias=True):

    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input")

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=bias), name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=bias), name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    conv05 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv05")(relu04)
    pool05 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool05")(conv05)
    norm05 = TimeDistributed(BatchNormalization(), name="conv_batch_norm05")(pool05)
    relu05 = TimeDistributed(Activation("relu"), name="conv_relu05")(norm05)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu05)

    img_dens01 = TimeDistributed(Dense(1024, use_bias=bias), name="img_dense01")(flatten)
    img_dens_norm01 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm01")(img_dens01)
    img_dens_tanh01 = TimeDistributed(Activation("tanh"), name="img_dense_tanh01")(img_dens_norm01)
    img_dens_drop01 = TimeDistributed(Dropout(0.5), name="img_dense_dropout01")(img_dens_tanh01)

    img_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="img_dense02")(img_dens_drop01)
    img_dens_norm02 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm02")(img_dens02)
    img_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02")(img_dens_norm02)
    img_dens_drop02 = TimeDistributed(Dropout(0.5), name="img_dense_dropout02")(img_dens_tanh02)

    img_dens03 = TimeDistributed(Dense(30, use_bias=bias), name="img_dense03")(img_dens_drop02)
    img_dens_norm03 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm03")(img_dens03)
    img_dens_tanh03 = TimeDistributed(Activation("tanh"), name="img_dense_tanh03")(img_dens_norm03)
    img_dens_drop03 = TimeDistributed(Dropout(0.5), name="img_dense_dropout03")(img_dens_tanh03)
    
    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs")(img_dens_drop03)
    
    # SPEED PART
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, name="lstm01")(speed_inputs)

    speed_dens01 = TimeDistributed(Dense(50, use_bias=bias), name="speed_dense01")(lstm01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01")(speed_dens_norm01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.5), name="speed_dense_drop01")(speed_dens_tanh01)

    speed_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="speed_dense02")(speed_dens_drop01)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh02")(speed_dens_norm02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.5), name="speed_dense_drop02")(speed_dens_tanh02)

    concat = Concatenate(name="concat")([img_dens_drop02, speed_dens_drop02])

    output_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="output_dens02")(concat)
    output_dens_norm02 = TimeDistributed(BatchNormalization(), name="output_dens_norm02")(output_dens02)
    output_dens_tanh02 = TimeDistributed(Activation("tanh"), name="output_dens_tanh02")(output_dens_norm02)
    output_dens_drop02 = TimeDistributed(Dropout(0.5), name="output_dens_drop02")(output_dens_tanh02)

    speed_outputs = TimeDistributed(Dense(1, activation='relu'), name="speed_outputs")(output_dens_drop02)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, steer_inputs, speed_inputs], outputs=[steer_outputs, speed_outputs])

    if compile_model:
        model.compile(loss={"steer_outputs": "mae", "speed_outputs": "mae"},
                      optimizer=optimizer,
                      metrics={"steer_outputs": "mae", "speed_outputs": "mae"},
                      sample_weight_mode="temporal")

    return model


def fabian_ket_cnn_research_b_speed_paper(image_shape, batch_size=1, compile_model=True, optimizer="adam", bias=True):

    img_batch_shape   = (None, image_shape[0], image_shape[1], image_shape[2])

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")

    # IMG PART
    conv01 = Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=bias, name="conv01")(img_inputs)
    pool01 = MaxPooling2D(pool_size=(2, 2), name="pool01")(conv01)
    norm01 = BatchNormalization(name="conv_batch_norm01")(pool01)
    relu01 = Activation("relu", name="conv_relu01")(norm01)

    conv02 = Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=bias, name="conv02")(relu01)
    pool02 = MaxPooling2D(pool_size=(2, 2), name="pool02")(conv02)
    norm02 = BatchNormalization(name="conv_batch_norm02")(pool02)
    relu02 = Activation("relu", name="conv_relu02")(norm02)

    conv03 = Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv03")(relu02)
    pool03 = MaxPooling2D(pool_size=(2, 2), name="pool03")(conv03)
    norm03 = BatchNormalization(name="conv_batch_norm03")(pool03)
    relu03 = Activation("relu", name="conv_relu03")(norm03)

    conv04 = Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv04")(relu03)
    pool04 = MaxPooling2D(pool_size=(2, 2), name="pool04")(conv04)
    norm04 = BatchNormalization(name="conv_batch_norm04")(pool04)
    relu04 = Activation("relu", name="conv_relu04")(norm04)

    conv05 = Conv2D(filters=256, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv05")(relu04)
    pool05 = MaxPooling2D(pool_size=(2, 2), name="pool05")(conv05)
    norm05 = BatchNormalization(name="conv_batch_norm05")(pool05)
    relu05 = Activation("relu", name="conv_relu05")(norm05)

    flatten = Flatten(name="flatten")(relu05)

    img_dens01 = Dense(1024, use_bias=bias, name="img_dense01")(flatten)
    img_dens_norm01 = BatchNormalization(name="img_dense_batch_norm01")(img_dens01)
    img_dens_tanh01 = Activation("tanh", name="img_dense_tanh01")(img_dens_norm01)
    img_dens_drop01 = Dropout(0.5, name="img_dense_dropout01")(img_dens_tanh01)

    img_dens02 = Dense(50, use_bias=bias, name="img_dense02")(img_dens_drop01)
    img_dens_norm02 = BatchNormalization(name="img_dense_batch_norm02")(img_dens02)
    img_dens_tanh02 = Activation("tanh", name="img_dense_tanh02")(img_dens_norm02)
    img_dens_drop02 = Dropout(0.5, name="img_dense_dropout02")(img_dens_tanh02)

    img_dens03 = Dense(30, use_bias=bias, name="img_dense03")(img_dens_drop02)
    img_dens_norm03 = BatchNormalization(name="img_dense_batch_norm03")(img_dens03)
    img_dens_tanh03 = Activation("tanh", name="img_dense_tanh03")(img_dens_norm03)
    img_dens_drop03 = Dropout(0.5, name="img_dense_dropout03")(img_dens_tanh03)
    
    speed_outputs = Dense(3, activation='softmax', name="speed_outputs")(img_dens_drop03)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs], outputs=[speed_outputs])

    if compile_model:
        model.compile(loss={"speed_outputs": "categorical_crossentropy"},
                      optimizer=optimizer,
                      metrics={"speed_outputs": "accuracy"},)
                      # sample_weight_mode="temporal")

    return model


def fabian_ket_cnn_research_b_steer_paper(image_shape, batch_size=1, compile_model=True, optimizer="adam", bias=True):

    img_batch_shape   = (None, image_shape[0], image_shape[1], image_shape[2])

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")

    # IMG PART
    conv01 = Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=bias, name="conv01")(img_inputs)
    pool01 = MaxPooling2D(pool_size=(2, 2), name="pool01")(conv01)
    norm01 = BatchNormalization(name="conv_batch_norm01")(pool01)
    relu01 = Activation("relu", name="conv_relu01")(norm01)

    conv02 = Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=bias, name="conv02")(relu01)
    pool02 = MaxPooling2D(pool_size=(2, 2), name="pool02")(conv02)
    norm02 = BatchNormalization(name="conv_batch_norm02")(pool02)
    relu02 = Activation("relu", name="conv_relu02")(norm02)

    conv03 = Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv03")(relu02)
    pool03 = MaxPooling2D(pool_size=(2, 2), name="pool03")(conv03)
    norm03 = BatchNormalization(name="conv_batch_norm03")(pool03)
    relu03 = Activation("relu", name="conv_relu03")(norm03)

    conv04 = Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv04")(relu03)
    pool04 = MaxPooling2D(pool_size=(2, 2), name="pool04")(conv04)
    norm04 = BatchNormalization(name="conv_batch_norm04")(pool04)
    relu04 = Activation("relu", name="conv_relu04")(norm04)

    conv05 = Conv2D(filters=256, kernel_size=(3, 3), padding="same", use_bias=bias, name="conv05")(relu04)
    pool05 = MaxPooling2D(pool_size=(2, 2), name="pool05")(conv05)
    norm05 = BatchNormalization(name="conv_batch_norm05")(pool05)
    relu05 = Activation("relu", name="conv_relu05")(norm05)

    flatten = Flatten(name="flatten")(relu05)

    img_dens01 = Dense(1024, use_bias=bias, name="img_dense01")(flatten)
    img_dens_norm01 = BatchNormalization(name="img_dense_batch_norm01")(img_dens01)
    img_dens_tanh01 = Activation("tanh", name="img_dense_tanh01")(img_dens_norm01)
    img_dens_drop01 = Dropout(0.5, name="img_dense_dropout01")(img_dens_tanh01)

    img_dens02 = Dense(50, use_bias=bias, name="img_dense02")(img_dens_drop01)
    img_dens_norm02 = BatchNormalization(name="img_dense_batch_norm02")(img_dens02)
    img_dens_tanh02 = Activation("tanh", name="img_dense_tanh02")(img_dens_norm02)
    img_dens_drop02 = Dropout(0.5, name="img_dense_dropout02")(img_dens_tanh02)

    img_dens03 = Dense(30, use_bias=bias, name="img_dense03")(img_dens_drop02)
    img_dens_norm03 = BatchNormalization(name="img_dense_batch_norm03")(img_dens03)
    img_dens_tanh03 = Activation("tanh", name="img_dense_tanh03")(img_dens_norm03)
    img_dens_drop03 = Dropout(0.5, name="img_dense_dropout03")(img_dens_tanh03)
    
    steer_outputs = Dense(1, activation='tanh', name="steer_outputs")(img_dens_drop03)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs], outputs=[steer_outputs])

    if compile_model:
        model.compile(loss={"steer_outputs": "mae"},
                      optimizer=optimizer,
                      metrics={"steer_outputs": "mae"},)
                      # sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_paper(image_shape, batch_size=1, stateful=True,
                                           compile_model=True, optimizer="adam", bias=True):

    sequence_size = None
    img_batch_shape = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 3)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input")

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=bias), name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=bias), name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    conv05 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv05")(relu04)
    pool05 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool05")(conv05)
    norm05 = TimeDistributed(BatchNormalization(), name="conv_batch_norm05")(pool05)
    relu05 = TimeDistributed(Activation("relu"), name="conv_relu05")(norm05)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu05)

    img_dens01 = TimeDistributed(Dense(1024, use_bias=bias), name="img_dense01")(flatten)
    img_dens_norm01 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm01")(img_dens01)
    img_dens_tanh01 = TimeDistributed(Activation("tanh"), name="img_dense_tanh01")(img_dens_norm01)
    img_dens_drop01 = TimeDistributed(Dropout(0.5), name="img_dense_dropout01")(img_dens_tanh01)

    img_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="img_dense02")(img_dens_drop01)
    img_dens_norm02 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm02")(img_dens02)
    img_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02")(img_dens_norm02)
    img_dens_drop02 = TimeDistributed(Dropout(0.5), name="img_dense_dropout02")(img_dens_tanh02)

    # SPEED PART
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, name="lstm01")(speed_inputs)

    speed_dens01 = TimeDistributed(Dense(50, use_bias=bias), name="speed_dense01")(lstm01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01")(speed_dens_norm01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.5), name="speed_dense_drop01")(speed_dens_tanh01)

    speed_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="speed_dense02")(speed_dens_drop01)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh02")(speed_dens_norm02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.5), name="speed_dense_drop02")(speed_dens_tanh02)

    concat = Concatenate(name="concat")([img_dens_drop02, speed_dens_drop02])

    output_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="output_dens02")(concat)
    output_dens_norm02 = TimeDistributed(BatchNormalization(), name="output_dens_norm02")(output_dens02)
    output_dens_tanh02 = TimeDistributed(Activation("tanh"), name="output_dens_tanh02")(output_dens_norm02)
    output_dens_drop02 = TimeDistributed(Dropout(0.5), name="output_dens_drop02")(output_dens_tanh02)

    speed_outputs = TimeDistributed(Dense(3, activation='softmax'), name="speed_outputs")(output_dens_drop02)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])

    if compile_model:
        model.compile(loss={"speed_outputs": "categorical_crossentropy"},
                      optimizer=optimizer,
                      metrics={"speed_outputs": "categorical_accuracy"},
                      sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_paper_seq(image_shape, batch_size=1, stateful=True,
                                               compile_model=True, optimizer="adam", bias=True):

    sequence_size = None
    img_batch_shape = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 50)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input")

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), padding="same", use_bias=bias), name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(norm01)

    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), padding="same", use_bias=bias), name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(norm02)

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(norm03)

    conv04 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(norm04)

    conv05 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv05")(relu04)
    pool05 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool05")(conv05)
    norm05 = TimeDistributed(BatchNormalization(), name="conv_batch_norm05")(pool05)
    relu05 = TimeDistributed(Activation("relu"), name="conv_relu05")(norm05)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu05)

    img_dens01 = TimeDistributed(Dense(1024, use_bias=bias), name="img_dense01")(flatten)
    img_dens_norm01 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm01")(img_dens01)
    img_dens_tanh01 = TimeDistributed(Activation("tanh"), name="img_dense_tanh01")(img_dens_norm01)
    img_dens_drop01 = TimeDistributed(Dropout(0.5), name="img_dense_dropout01")(img_dens_tanh01)

    img_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="img_dense02")(img_dens_drop01)
    img_dens_norm02 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm02")(img_dens02)
    img_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02")(img_dens_norm02)
    img_dens_drop02 = TimeDistributed(Dropout(0.5), name="img_dense_dropout02")(img_dens_tanh02)

    # SPEED PART
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, name="lstm01")(speed_inputs)

    speed_dens01 = TimeDistributed(Dense(50, use_bias=bias), name="speed_dense01")(lstm01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01")(speed_dens_norm01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.5), name="speed_dense_drop01")(speed_dens_tanh01)

    speed_dens02 = TimeDistributed(Dense(25, use_bias=bias), name="speed_dense02")(speed_dens_drop01)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh02")(speed_dens_norm02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.5), name="speed_dense_drop02")(speed_dens_tanh02)

    concat = Concatenate(name="concat")([img_dens_drop02, speed_dens_drop02])

    output_dens02 = TimeDistributed(Dense(50, use_bias=bias), name="output_dens02")(concat)
    output_dens_norm02 = TimeDistributed(BatchNormalization(), name="output_dens_norm02")(output_dens02)
    output_dens_tanh02 = TimeDistributed(Activation("tanh"), name="output_dens_tanh02")(output_dens_norm02)
    output_dens_drop02 = TimeDistributed(Dropout(0.5), name="output_dens_drop02")(output_dens_tanh02)

    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs")(output_dens_drop02)
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])

    if compile_model:
        model.compile(loss={"speed_outputs": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs": "mae"},
                      sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_herta(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input")
    

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=bias), name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    # norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(pool01)

    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    # norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(pool02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    # norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(pool03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    # norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(pool04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)
    
    speed_conc01 = Concatenate(name="speed_conc01")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(32, use_bias=bias), name="speed_dense01")(speed_conc01)
    # img_dens_norm01 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm01")(img_dens01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01")(speed_dens01)
    # speed_dens_drop01 = TimeDistributed(Dropout(0.5), name="img_dense_dropout01")(img_dens_tanh01)
    
    speed_conc02 = Concatenate(name="speed_conc02")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(32, use_bias=bias), name="speed_dense02")(speed_conc02)
    # img_dens_norm02 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm02")(img_dens02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02")(speed_dens02)
    # img_dens_drop02 = TimeDistributed(Dropout(0.5), name="img_dense_dropout02")(img_dens_tanh02)
    
    speed_conc03 = Concatenate(name="speed_conc03")([speed_dens_tanh02, speed_inputs])
    
    lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="lstm01")(speed_conc03)
    
    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])

    if compile_model:
        model.compile(loss={"speed_outputs": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs": "mae"},
                      sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_herta2(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input")
    

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), padding="same", use_bias=bias), name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    # norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(pool01)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01")(pool01)

    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), padding="same", use_bias=bias), name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    # norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(pool02)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02")(pool02)

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    # norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(pool03)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03")(pool03)

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), padding="same", use_bias=bias), name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    # norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(pool04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04")(pool04)

    flatten = TimeDistributed(Flatten(), name="flatten")(relu04)
    
    # speed_conc01 = Concatenate(name="speed_conc01")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(32, use_bias=bias), name="speed_dense01")(flatten)
    # img_dens_norm01 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm01")(img_dens01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01")(speed_dens01)
    # speed_dens_drop01 = TimeDistributed(Dropout(0.5), name="img_dense_dropout01")(img_dens_tanh01)
    
    # speed_conc02 = Concatenate(name="speed_conc02")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(32, use_bias=bias), name="speed_dense02")(speed_dens_tanh01)
    # img_dens_norm02 = TimeDistributed(BatchNormalization(), name="img_dense_batch_norm02")(img_dens02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02")(speed_dens02)
    # img_dens_drop02 = TimeDistributed(Dropout(0.5), name="img_dense_dropout02")(img_dens_tanh02)
    
    # speed_conc03 = Concatenate(name="speed_conc03")([speed_dens_tanh02, speed_inputs])
    
    lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="lstm01")(speed_dens_tanh02)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs])

    if compile_model:
        model.compile(loss={"speed_outputs": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs": "mae"},
                      sample_weight_mode="temporal")

    return model


def fabian_ket_lstm_research_b_speed_herta_norm(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=32, kernel_size=(11, 11), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv01_gen")(img_inputs)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01_gen")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(relu01)
    
    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv02_gen")(norm01)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02_gen")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(relu02)
    

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), 
                                    strides=(2, 2) ,padding="same", 
                                    use_bias=bias), name="conv03_gen")(norm02)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03_gen")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(relu03)
    

    conv04 = TimeDistributed(Conv2D(filters=32, kernel_size=(3, 3), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv04_gen")(norm03)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(relu04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(norm04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(32, activation="tanh", use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    # speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens01)
    # speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="img_dense_dropout01")(speed_dens01)
    
    
    speed_dens_drop01.set_shape((batch_size, sequence_size, speed_dens_drop01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_drop01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(32, activation="tanh", use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    # speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens02)
    # speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    
    speed_dens_drop02.set_shape((batch_size, sequence_size, speed_dens_drop02.shape[2]))
    #speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_drop02, speed_inputs])
    
    #speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(32, return_sequences=True, return_state=False, stateful=stateful, name="lstm01_gen")(speed_dens_drop02)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

#     if compile_model:
#         model.compile(loss={"speed_outputs_gen": "mae"},
#                       optimizer=optimizer,
#                       metrics={"speed_outputs_gen": "mae"},
#                       sample_weight_mode="temporal")

    return model


def generator_model3(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=64, kernel_size=(11, 11), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv01_gen")(img_inputs)
    relu01 = TimeDistributed(Activation("relu"), name="conv_relu01_gen")(conv01)
    norm01 = TimeDistributed(BatchNormalization(), name="conv_batch_norm01")(relu01)
    
    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(5, 5), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv02_gen")(norm01)
    relu02 = TimeDistributed(Activation("relu"), name="conv_relu02_gen")(conv02)
    norm02 = TimeDistributed(BatchNormalization(), name="conv_batch_norm02")(relu02)
    

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), 
                                    strides=(2, 2) ,padding="same", 
                                    use_bias=bias), name="conv03_gen")(norm02)
    relu03 = TimeDistributed(Activation("relu"), name="conv_relu03_gen")(conv03)
    norm03 = TimeDistributed(BatchNormalization(), name="conv_batch_norm03")(relu03)
    

    conv04 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(2, 2), padding="same", 
                                    use_bias=bias), name="conv04_gen")(norm03)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(conv04)
    norm04 = TimeDistributed(BatchNormalization(), name="conv_batch_norm04")(relu04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(norm04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(256, activation="tanh", use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    # speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens01)
    # speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="img_dense_dropout01")(speed_dens01)
    
    
    speed_dens_drop01.set_shape((batch_size, sequence_size, speed_dens_drop01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_drop01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(50, activation="tanh", use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    # speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens02)
    # speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    
    speed_dens_drop02.set_shape((batch_size, sequence_size, speed_dens_drop02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_drop02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(50, return_sequences=True, return_state=False, stateful=stateful, name="lstm01_gen")(speed_conc03)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

#     if compile_model:
#         model.compile(loss={"speed_outputs_gen": "mae"},
#                       optimizer=optimizer,
#                       metrics={"speed_outputs_gen": "mae"},
#                       sample_weight_mode="temporal")

    return model


# https://machinelearningmastery.com/how-to-reduce-overfitting-in-deep-learning-with-weight-regularization/

def generator_model4(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=64, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(256, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


def generator_model4_steer(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=64, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(256, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


def generator_model5(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=48, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=80, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(160, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(96, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


# https://machinelearningmastery.com/how-to-reduce-overfitting-in-deep-learning-with-weight-regularization/

def generator_model4_only_img_steer(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=64, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=96, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=128, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    # speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(256, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(flatten)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    # speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_dens_tanh01)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    # speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    # speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_dens_tanh02)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


def generator_model5_steer(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=48, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=64, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=96, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=80, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(160, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(96, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='tanh'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


def generator_model6(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs], outputs=[speed_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model


def generator_model6_both(image_shape, batch_size=1, stateful=True, compile_model=True, optimizer="adam", bias=True):
    
    sequence_size = None
    img_batch_shape   = (batch_size, sequence_size, image_shape[0], image_shape[1], image_shape[2])
    speed_batch_shape = (batch_size, sequence_size, 1)
    steer_batch_shape = (batch_size, sequence_size, 1)

    # ARCHITECTURE BEGIN
    img_inputs = Input(batch_shape=img_batch_shape, name="img_input_gen")
    speed_inputs = Input(batch_shape=speed_batch_shape, name="speed_input_gen")
    steer_inputs = Input(batch_shape=steer_batch_shape, name="steer_input_gen")
    
    
    cnn_l2 = 0.01
    dense_l2 = 0.01
    lstm_l2 = 0.01

    # IMG PART
    conv01 = TimeDistributed(Conv2D(filters=96, kernel_size=(11, 11), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv01")(img_inputs)
    pool01 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool01")(conv01)
    drop01 = TimeDistributed(Dropout(0.2), name="drop01")(pool01)
    norm01 = TimeDistributed(BatchNormalization(), name="norm01")(drop01)
    relu01 = TimeDistributed(Activation("relu"), name="relu01")(norm01)
    
    
    
    conv02 = TimeDistributed(Conv2D(filters=256, kernel_size=(5, 5), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv02")(relu01)
    pool02 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool02")(conv02)
    drop02 = TimeDistributed(Dropout(0.2), name="drop02")(pool02)
    norm02 = TimeDistributed(BatchNormalization(), name="norm02")(drop02)
    relu02 = TimeDistributed(Activation("relu"), name="relu02")(norm02)
    
    

    conv03 = TimeDistributed(Conv2D(filters=384, kernel_size=(3, 3), 
                                    strides=(1, 1) ,padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv03")(relu02)
    pool03 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool03")(conv03)
    drop03 = TimeDistributed(Dropout(0.2), name="drop03")(pool03)
    norm03 = TimeDistributed(BatchNormalization(), name="norm03")(drop03)
    relu03 = TimeDistributed(Activation("relu"), name="relu03")(norm03)
    
    

    conv04 = TimeDistributed(Conv2D(filters=256, kernel_size=(3, 3), 
                                    strides=(1, 1), padding="same", 
                                    use_bias=bias, activity_regularizer=l2(cnn_l2)), 
                             name="conv04")(relu03)
    pool04 = TimeDistributed(MaxPooling2D(pool_size=(2, 2)), name="pool04")(conv04)
    drop04 = TimeDistributed(Dropout(0.2), name="drop04")(pool04)
    norm04 = TimeDistributed(BatchNormalization(), name="norm04")(drop04)
    relu04 = TimeDistributed(Activation("relu"), name="conv_relu04_gen")(norm04)
    

    flatten = TimeDistributed(Flatten(), name="flatten_gen")(relu04)
    flatten.set_shape((batch_size, sequence_size, flatten.shape[2]))
    
    speed_conc01 = Concatenate(name="speed_conc01_gen")([flatten, speed_inputs, steer_inputs])

    speed_dens01 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense01_gen")(speed_conc01)
    speed_dens_drop01 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout01")(speed_dens01)
    speed_dens_norm01 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm01")(speed_dens_drop01)
    speed_dens_tanh01 = TimeDistributed(Activation("tanh"), name="speed_dense_tanh01_gen")(speed_dens_norm01)
    
    
    speed_dens_tanh01.set_shape((batch_size, sequence_size, speed_dens_tanh01.shape[2]))
    speed_conc02 = Concatenate(name="speed_conc02_gen")([speed_dens_tanh01, speed_inputs, steer_inputs])
    
    speed_dens02 = TimeDistributed(Dense(128, activity_regularizer=l2(dense_l2), 
                                         activation=None, use_bias=bias), name="speed_dense02_gen")(speed_conc02)
    speed_dens_drop02 = TimeDistributed(Dropout(0.2), name="speed_dense_dropout02")(speed_dens02)
    speed_dens_norm02 = TimeDistributed(BatchNormalization(), name="speed_dense_batch_norm02")(speed_dens_drop02)
    speed_dens_tanh02 = TimeDistributed(Activation("tanh"), name="img_dense_tanh02_gen")(speed_dens_norm02)
    
    
    speed_dens_tanh02.set_shape((batch_size, sequence_size, speed_dens_tanh02.shape[2]))
    speed_conc03 = Concatenate(name="speed_conc03_gen")([speed_dens_tanh02, speed_inputs, steer_inputs])
    
    speed_conc03.set_shape((batch_size, sequence_size, speed_conc03.shape[2]))
    lstm01 = LSTM(128, return_sequences=True, return_state=False, stateful=stateful, 
                  activity_regularizer=l2(lstm_l2), recurrent_regularizer=l2(lstm_l2), 
                  name="lstm01_gen")(speed_conc03)
    # speed_lstm_norm01 = TimeDistributed(BatchNormalization(), name="speed_lstm_batch_norm01")(lstm01)
    
    speed_outputs = TimeDistributed(Dense(1, activation='sigmoid'), name="speed_outputs_gen")(lstm01)
    steer_outputs = TimeDistributed(Dense(1, activation='tanh'), name="steer_outputs_gen")(lstm01)
    
    # ARCHITECTURE END

    model = Model(inputs=[img_inputs, speed_inputs, steer_inputs], outputs=[speed_outputs, steer_outputs], name="generator")

    if compile_model:
        model.compile(loss={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      optimizer=optimizer,
                      metrics={"speed_outputs_gen": "mae", "steer_outputs_gen": "mae"},
                      sample_weight_mode="temporal")

    return model
