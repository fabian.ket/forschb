import cv2
import numpy as np
import time
import os
import multiprocessing as mp

# own  imports
import Src.YamlUtils as yu

        
def process_data(tup):
    idx, img, steer, speed, save_dir, folder = tup
    
    dic = {}
    
    timestamp = idx
        
    depth = img.shape[0]
    height = img.shape[1]
    width = img.shape[2]

    imgNew = np.zeros((height, width, depth))

    # np.reshape() is not working
    # breaks color and img
    # one img transforms to 3x3 single images img
    # seems like only every 3'th pixel is used
    #
    # it is nine times nearly the same img
    # single img becomes the following:
    #
    # +--------------------+       +------+------+------+
    # |                    |       |      |      |      |
    # |                    |       +------+------+------+
    # |                    |  ==>  |      |      |      |
    # |                    |       +------+------+------+
    # |                    |       |      |      |      |
    # +--------------------+       +------+------+------+
    #
    # quick fix, use two for's
    #

    for x in range(width):
        for y in range(height):
            imgNew[y][x][0] = img[2][y][x] # B
            imgNew[y][x][1] = img[1][y][x] # G 
            imgNew[y][x][2] = img[0][y][x] # R

    dic[timestamp] = {"steering": float(steer), "speed": float(speed)}

    cv2.imwrite("{}/{}/{}.png".format(save_dir, folder, timestamp), imgNew)
    
    # print("{}/{}/{}.png".format(save_dir, folder, timestamp))
    
    return dic
    
def comma_ai_stuff_to_usable_data(imgs, steerings, speeds, save_dir, folder="imgs"):
    
    t0 = time.time()
    
    result = {}
    
    path = "{}/{}".format(save_dir, folder)
    
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == os.errno.EEXIST and os.path.isdir(path):
            pass
        else:
            print("Could not create dir {}".format(path))
            raise
    
    indices = range(len(imgs))
    save_dirs = [save_dir for i in range(len(imgs))]
    folders = [folder for i in range(len(imgs))]
    
    with mp.Pool() as pool:
        dics = pool.map(
            process_data, 
            zip(indices, imgs, steerings, speeds, save_dirs, folders)
        )
    
    
    for dic in dics:
        result.update(dic)
    
    yu.write_dict_to_yaml(save_dir + "/training.yaml", result)
    
    print("Processing time: {}sec".format(time.time() - t0))

    