#!/usr/bin/env python

import abc
from abc import abstractmethod

# compatible with Python 2 *and* 3:
ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()}) 

class Msg(ABC):

    PREDICT = 0
    PREDICTION = 1

    @abstractmethod
    def type(self):
        raise NotImplementedError("Msg type() not implemented")

class PredictMsg(Msg):

    def __init__(self, x):
        self.x = x

    def type(self):
        return Msg.PREDICT

class PrecitionMsg(Msg):

    def __init__(self, prediction):
        self.prediction = prediction

    def type(self):
        return Msg.PREDICTION
