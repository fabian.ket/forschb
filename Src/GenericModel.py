#!/usr/bin/env python

from abc import ABC, abstractmethod

class AbstractModel(ABC):
    def __init__(self, model, batch_size, img_shape):
        self._internal_model = model
        self._batch_size = batch_size
        self._img_shape = img_shape
    
    def copy(self, model):
        self._internal_model = model._internal_model
        self._img_shape = model._img_shape
    
    @abstractmethod
    def set_internal_tensors(self):
        raise NotImplementedError
        
    @abstractmethod
    def set_internal_values(self, tf_session):
        raise NotImplementedError
    
    @abstractmethod    
    def assign_internal_values(self, tf_session):
        raise NotImplementedError
    
    @abstractmethod    
    def _prepare(self):
        raise NotImplementedError
    
    @abstractmethod
    def reset_states(self):
        raise NotImplementedError
    
    def predict(self, x, batch_size):
        self._prepare()
        return self._internal_model.predict(x, batch_size=batch_size)
        
    def _get_layer_by_name(self, layer_name):
        layer = None
        
        for l in self._internal_model.layers:
            if layer_name in l.name:
                layer = l
                break
                
        return layer
        
    def get_img_shape(self):
        return self._img_shape
        
    def get_batch_size(self):
        return self._batch_size
        
        
class SeqModel(AbstractModel):
    def __init__(self, model, batch_size, img_shape, rnn_layer_name="lstm"):
        super(SeqModel, self).__init__(model=model, batch_size=batch_size, img_shape=img_shape)
        
        self._rnn_layer_name = rnn_layer_name
        
        self._cell_tensor = None
        self._hidden_tensor = None
        
        self._cell_state = None
        self._hidden_state = None
        
    def set_internal_tensors(self):
        rnn = self._get_layer_by_name(self._rnn_layer_name)
        self._cell_tensor, self._hidden_tensor = rnn.states
        
    def set_internal_values(self, tf_session):
        self._cell_state   = self._cell_tensor.eval(tf_session)
        self._hidden_state = self._hidden_tensor.eval(tf_session)
        
    def assign_internal_values(self, tf_session):
        assign_op = self._cell_tensor.assign(self._cell_state)
        tf_session.run(assign_op)
        assign_op = self._hidden_tensor.assign(self._hidden_state)
        tf_session.run(assign_op)

    def _prepare(self):
        pass
        
    def reset_states(self):
        self._internal_model.reset_states()
        
        
class NonSeqModel(AbstractModel):
    def set_internal_tensors(self):
        pass
        
    def set_internal_values(self, tf_session):
        pass
        
    def assign_internal_values(self, tf_session):
        pass
        
    def _prepare(self):
        pass
    
    def reset_states(self):
        pass