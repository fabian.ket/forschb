#!/usr/bin/env python3

def main():
    # tensorflow GPU growth config
    import tensorflow as tf

    gpu = tf.config.experimental.list_physical_devices("GPU")
    print(gpu)

    tf.config.experimental.set_memory_growth(gpu[0], True)


    # sys imports
    import numpy as np
    import os
    import copy

    # own imports
    import Src.YamlUtils as yu
    import Src.DicData as dade
    import Src.Slideshow as slide
    import Src.KerasSequence as ks
    import Src.ImgProcessing as img_proc
    import Src.OversampleStreamData as osa
    import Src.Speed as speed
    import Src.Models as models
    import Src.KerasCallbacks as kc
    import Src.Models as models
    import Src.SequenceModels as seq_models
    import Src.ModelBuilder as mb



    # load yaml paths
    img_path = "/home/s0549108/forschB/datasets/neurorace_sim/forschB_data/{}/depth_camera/rgb/image_raw/compressed"
    train_path = "/home/s0549108/forschB/datasets/neurorace_sim/forschB_data"

    valid_img_path = "/home/s0549108/forschB/datasets/neurorace_sim/forschB_data_counter/{}/depth_camera/rgb/image_raw/compressed"
    valid_path = "/home/s0549108/forschB/datasets/neurorace_sim/forschB_data_counter"


    dirs = os.listdir(train_path)
    valid_dirs = os.listdir(valid_path)


    yamls = []

    for d in dirs:
        yamls.append("{}/{}/{}.yaml".format(train_path, d, d))


    valid_yamls = []

    for d in valid_dirs:
        valid_yamls.append("{}/{}/{}.yaml".format(valid_path, d, d))


    # read yamls and create dicts
    print("Read yamls ...")
    
    dicts = []

    for y in yamls:
        dicts.append(dade.convert_neuro_data_to_yaml(y))

    valid_dicts = []

    for y in valid_yamls:
        valid_dicts.append(dade.convert_neuro_data_to_yaml(y))
    
    print("... done")

    def do_all(dic):
        # dade.remove_data_with_speed_lower_than_threshold(dic)
        max_abs_speed, max_abs_steer = dade.get_max_abs_speed_and_steering(dic)

        # dade.add_speed_classes_to_dic(dic, threshold=0.05)

        # dade.normalize_dic_steering(dic, max_abs_steer)
        dade.normalize_dic_speed(dic, 10)

        dade.add_speed_classes_to_dic_like_steering(dic)

        for k in dic.keys():

            if "speed_class" not in dic[k].keys():
                print(k)
                print(type(k))


        dade.add_steer_classes_to_dic(dic)
        dade.add_speed_cost_weight_to_dic(dic)
        dade.add_steer_cost_weight_to_dic_classifier(dic)

        return max_abs_steer, max_abs_speed


    max_steers = []
    max_speeds = []

    for d in dicts:
        max_steer, max_speed = do_all(d)
        max_steers.append(max_steer)
        max_speeds.append(max_speed)
        
    for d in valid_dicts:
        _, _ = do_all(d)
    

    def every_second_key(dicts):
    
        result = []
        
        for dic in dicts:
        
            r = {}
            
            doit = True
            
            for key in dic.keys():
                
                if doit:
                    r[key] = copy.deepcopy(dic[key])
                    doit = False
                    
                else:
                    doit = True
                    
            result.append(r)
                
        return result
    
    
    every_second_key_dicts = every_second_key(dicts)
    every_fourth_key_dicts = every_second_key(every_second_key_dicts)
    
    
    over_half_dicts = []

    class_key = "steering_class"

    for dic in every_second_key_dicts:
        analyzed_key_lists = osa.analyze_class_in_dic(dic, class_key, verbose=0)
        od = osa.oversample_class_in_dic_sorted(dic, analyzed_key_lists, class_key, max_repeat=10, verbose=0)
        
        over_half_dicts.append(od)
    
    
    # DATA GENS
    img_processor = img_proc.ImgProcessor()

    img_processor.crop = True
    img_processor.augment = False

    img_processor.fx = 0.1
    img_processor.fy = 0.2
    
    # ================================================
    # ================================================
    # ================================================
    #                SEQUENCE SIZE
    # ================================================
    # ================================================
    # ================================================
    
    batch_size = 1
    sequence_size = 16
    
    gen_in_use = ks.SequenceGeneratorRegression

    return_speed_input = True        
    return_steer_input = True

    return_speed_output = False        
    return_steer_output = True

    return_speed_weights = False
    return_steer_weights = False
    
    speed_gens = []

    for f, d in zip(dicts, dirs):
        
        gen = gen_in_use(f, img_path.format(d), img_processor, sequence_size=sequence_size, img_file_type="jpg")
        
        gen.return_speed_input = return_speed_input
        gen.return_steer_input = return_steer_input

        gen.return_speed_output = return_speed_output
        gen.return_steer_output = return_steer_output

        gen.return_speed_weights = return_speed_weights
        gen.return_steer_weights = return_steer_weights
        
        speed_gens.append(gen)
    

    dist_half_gens = []
    class_key = "steering_class"
    for dic in every_second_key_dicts:
        analyzed_key_lists = osa.analyze_class_in_dic(dic, class_key, verbose=0)
        dist_half_gens.append(analyzed_key_lists)
    
    
    half_gens = []
    for f, d, dist in zip(every_second_key_dicts, dirs, dist_half_gens):
        
        gen = gen_in_use(f, img_path.format(d), img_processor, sequence_size=sequence_size, img_file_type="jpg", class_dist=dist, class_key=class_key)
        
        gen.return_speed_input = return_speed_input
        gen.return_steer_input = return_steer_input

        gen.return_speed_output = return_speed_output
        gen.return_steer_output = return_steer_output

        gen.return_speed_weights = return_speed_weights
        gen.return_steer_weights = return_steer_weights
        
        half_gens.append(gen)
    
    quarter_speed_gens = []

    for f, d in zip(every_fourth_key_dicts, dirs):
        
        gen = gen_in_use(f, img_path.format(d), img_processor, sequence_size=sequence_size, img_file_type="jpg")
        
        gen.return_speed_input = return_speed_input
        gen.return_steer_input = return_steer_input

        gen.return_speed_output = return_speed_output
        gen.return_steer_output = return_steer_output

        gen.return_speed_weights = return_speed_weights
        gen.return_steer_weights = return_steer_weights
        
        quarter_speed_gens.append(gen)
    
    over_half_gens = []

    for f, d in zip(over_half_dicts, dirs):
        
        gen = gen_in_use(f, img_path.format(d), img_processor, sequence_size=sequence_size, img_file_type="jpg")
        
        gen.return_speed_input = return_speed_input
        gen.return_steer_input = return_steer_input

        gen.return_speed_output = return_speed_output
        gen.return_steer_output = return_steer_output

        gen.return_speed_weights = return_speed_weights
        gen.return_steer_weights = return_steer_weights
        
        over_half_gens.append(gen)
    
    valid_speed_gens = []

    for f, d in zip(valid_dicts, valid_dirs):
        
        gen = gen_in_use(f, valid_img_path.format(d), img_processor, sequence_size=sequence_size, img_file_type="jpg")
        
        gen.return_speed_input = return_speed_input
        gen.return_steer_input = return_steer_input

        gen.return_speed_output = return_speed_output
        gen.return_steer_output = return_steer_output

        gen.return_speed_weights = return_speed_weights
        gen.return_steer_weights = return_steer_weights
        
        valid_speed_gens.append(gen)
        
    
    batched_speed_gens = []
    gens_in_use = half_gens

    # ================================================
    # ================================================
    # ================================================
    #                  BATCH SIZE
    # ================================================
    # ================================================
    # ================================================
    
    batch_size = 20
    n_batched_gens = 20 // batch_size
    
    gen_in_use = ks.BatchedTrainigSequenceGenerator
    
    for i in range(n_batched_gens):
        batched_speed_gen = gen_in_use(gens_in_use[i * batch_size : (i+1) * batch_size])
        batched_speed_gens.append(batched_speed_gen)
    
    # raise 0
    
    
    # =======================================================
    # =======================================================
    # =======================================================
    #                        CONFIG
    # =======================================================
    # =======================================================
    # =======================================================
    
    model_number = "6_both_in_steer_out_seq16_minority_aug"
    gen_save_file   = "steer_eager_model{}.h5".format(model_number)
    valid_save_file = "steer_eager_valid_model{}_({})_stateless_half.h5".format(model_number, "{}")
    model_func = seq_models.generator_model6_both_in_steer_out
    
    # TRAINING
    def validate_model(model, gens):
        j = 0
        
        valid_error = 0.0
        
        i = 0
        
        for gen in gens:
            model.reset_states()

            pre_steer = 0.0
            error = 0.0

            print("Start with valid ...")

            for [img_batch, pre_speed_batch, _], [label_batch_st] in gen:
                for img_seq, pre_speed_seq, label_seq_st in zip(img_batch, pre_speed_batch, label_batch_st):
                    for img, pre_speed, label_st in zip(img_seq, pre_speed_seq, label_seq_st):

                        img = np.asarray( [[img]], dtype=np.float32 )
                        pre_speed = np.asarray( [[[pre_speed]]], dtype=np.float32 )
                        pre_steer = np.asarray( [[[pre_steer]]], dtype=np.float32 )

                        test_img0  = np.zeros_like(img)
                        test_img1  = np.ones_like(img)
                        test_speed = np.zeros_like(pre_speed)
                        test_steer = np.zeros_like(pre_steer)

                        prediction_st = model.predict([ img, pre_speed, pre_steer ], batch_size=1)
                        
                        prediction_st = np.asscalar(prediction_st)

                        pre_steer = prediction_st
                        
                        label_st = np.asscalar(label_st)
                        
                        st_error = abs(label_st - prediction_st)
                        
                        error += st_error
                        i += 1
                        
                        valid_error += st_error
                        
                print("-", end="")
            
            print("")
            print("")
            
            j +=1

        return valid_error / i

    def train(generator, gen, epochs=1, valid_model=None, valid_gens=None, init_valid_error=9999):
        
        # This method returns a helper function to compute cross entropy loss
        # cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        mae = tf.keras.losses.MeanAbsoluteError()

        def generator_loss(y_predicted, y_batch):
            return mae(y_predicted, y_batch)
        
        def generator_loss_both(y_predicted_sp, y_batch_sp, y_predicted_st, y_batch_st):
            return mae(y_predicted_sp, y_batch_sp) + mae(y_predicted_st, y_batch_st)
        
        # generator_optimizer = tf.keras.optimizers.Adam(1.5e-4, 0.5)
        # generator_optimizer = tf.keras.optimizers.Adam(1.5e-5, 0.5)
        # generator_optimizer = tf.keras.optimizers.Adam(0.001, 0.9)
        generator_optimizer = tf.keras.optimizers.Adam(0.0007, 0.9)
        # generator_optimizer = tf.keras.optimizers.Adam(0.00007, 0.9)
        # generator_optimizer = tf.keras.optimizers.Adam(1.5e-6, 0.9)
        
        # generator_optimizer = tf.keras.optimizers.SGD(learning_rate=0.07, momentum=0.9)
        
        batch_size = generator.get_batch_size()
        
        # y_previous = np.zeros((batch_size, 1, 1))
        
        generator.set_internal_tensors()
        
        valid_error = init_valid_error
        
        pure_teacher_forcing = False
        second_hand_teacher  = True
        
        for e in range(epochs):
        
            print("Start epoch: {} ###############################################".format(e))

            generator.reset_states()

            g_losses = []
            
            y_previous_st = np.zeros((batch_size, 1, 1))
            
            for [x_batch, pre_y_batch_sp, pre_y_batch_st], [y_batch_st] in gen:

                # value_gen_cell_tensor   = copy.deepcopy(generator._cell_tensor.numpy())
                # value_gen_hidden_tensor = copy.deepcopy(generator._hidden_tensor.numpy())

                x_batch = np.asarray(x_batch, dtype=np.float32)
                
                pre_y_batch_sp = np.asarray(pre_y_batch_sp, dtype=np.float32)
                pre_y_batch_st = np.asarray(pre_y_batch_st, dtype=np.float32)
                
                y_batch_st = np.asarray(y_batch_st, dtype=np.float32)
                
                if not pure_teacher_forcing:
                
                    if second_hand_teacher:
                        # prediction with teacher forcing
                        y_generated_st = generator.predict([x_batch, pre_y_batch_sp, pre_y_batch_st], batch_size=batch_size)

                    else:
                        # prediction in free loop
                        y_generated_st = None
                        
                        y_free_previos_st = y_previous_st

                        seq_size = x_batch.shape[1]
                        for idx in range(seq_size):

                            x_input = np.expand_dims(x_batch[:, idx, :], axis=1)
                            pre_speed_input = np.expand_dims(pre_y_batch_sp[:, idx, :], axis=1)
                            
                            y_free_previos_st = generator.predict([x_input, pre_speed_input, y_free_previos_st], batch_size=batch_size)

                            if y_generated_st is None:
                                y_generated_st = y_free_previos_st

                            else:
                                y_generated_st = np.concatenate((y_generated_st, y_free_previos_st), axis=1)

                # generator._cell_tensor.assign(value_gen_cell_tensor)
                # generator._hidden_tensor.assign(value_gen_hidden_tensor)
                
                if not pure_teacher_forcing:
                    # create previous y batch based on teacher forcing outputs
                    y_pres_st = y_generated_st[:, :-1, :]

                    np_y_previous_st = np.asarray(y_previous_st, dtype=np.float32)
                    
                    y_pres_st = np.concatenate((np_y_previous_st, y_pres_st), axis=1)

                    y_previous_st = np.expand_dims(y_generated_st[:, -1, :], axis=2)
                    
                else:
                    y_pres_st = pre_y_batch_st

                    y_previous_st = np.expand_dims(y_batch_st[:, -1, :], axis=2)
                
                with tf.GradientTape() as gen_tape:
                    
                    # predict current y
                    y_predicted_st = generator._internal_model([x_batch, pre_y_batch_sp, y_pres_st], training=True)
                    
                    g_loss = generator_loss(y_predicted_st, y_batch_st)
                    

                    gradients_of_generator = gen_tape.gradient(g_loss, generator._internal_model.trainable_variables)
                    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator._internal_model.trainable_variables))


                    g_losses.append( copy.deepcopy( np.mean(g_loss.numpy()) ) )
                    
                    # print("{} [G batch loss: {}]".format(e, np.mean(g_loss.numpy())))

                    print("-", end="")

            print("")
            print("{} [G loss: {}]".format(e, np.mean(g_losses)))
            print("")
            
            if valid_model is not None and valid_gens is not None:
                generator._internal_model.save_weights(gen_save_file)
                
                valid_model._internal_model.load_weights(gen_save_file)
                    
                new_valid_error = validate_model(valid_model, valid_gens)
                
                if new_valid_error <= valid_error:
                    valid_model._internal_model.save_weights(valid_save_file.format(new_valid_error))
                    
                    print("Valid model saved [old {}] [new {}]".format(valid_error, new_valid_error))
                    print("")
                    
                    valid_error = new_valid_error
                
                else:
                    print("Valid error: [old {}] [new {}]".format(valid_error, new_valid_error))
                    print("")
                
        return valid_error
    
    
    def build_generator():
        builder = mb.SeqModelBuilder(model_func, 
                                     "lstm",
                                     (64,64,3), 
                                     batch_size=batch_size,
                                     stateful=False,
                                     compile_model=False,
                                     optimizer="adam",
                                     bias=True,
                                     weights_file=None)
        
        model = builder.build()
        
        return model
    
    print("Build generator model ... ")
    generator = build_generator()
    
    print(generator._internal_model.summary())
    # raise 0
    
    
    def build_validator():
        builder = mb.SeqModelBuilder(model_func, 
                                     "lstm",
                                     (64,64,3), 
                                     batch_size=1,
                                     stateful=True,
                                     compile_model=False,
                                     optimizer="adam",
                                     bias=True,
                                     weights_file=None)
        
        model = builder.build()
        
        return model
    
    
    print("Valid generator model ... ")
    valid_generator = build_validator()
    
    
    init_valid_error = 999
    
    print("Load model with valid error: {}".format(init_valid_error))
    
    try :
        generator._internal_model.load_weights(valid_save_file.format(init_valid_error))
    
    except OSError:
        print("Could not load valid error model {}".format(init_valid_error))
    
    
    print("Start training ...")
    
    n_max_model_load_count = len(batched_speed_gens)
    max_model_load_counter = 0
    
    old_valid_error = init_valid_error
    
    for round in range(50):
        
        i = 1
        
        for gen in batched_speed_gens:
        
            print("{} BATCH GEN {} +++++++++++++++++++++++++++++++++++++++++++++++".format(round, i))
            init_valid_error = train(generator=generator, gen=gen, epochs=5, 
                                     valid_model=valid_generator, valid_gens=half_gens[-1:],
                                     init_valid_error=init_valid_error)
            
            if max_model_load_counter <= n_max_model_load_count:
                generator._internal_model.load_weights(valid_save_file.format(init_valid_error))
                max_model_load_counter += 1
                
            if old_valid_error != init_valid_error:
                old_valid_error = init_valid_error
                max_model_load_counter = 0
            
            i += 1
            
if __name__ == "__main__":
    main()
