import cv2
import numpy as np


class ImgProcessor:
    
    def __init__(self):

        self.grayscale = False
        self.flip = False
        self.resize = True
        self.crop = True
        self.normalize = True
        
        self.fx = 0.4
        self.fy = 1.2
    
    def process(self, img):
        
        if self.grayscale:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # grayscaling
            img = img[:, :, np.newaxis]
                
        img_shape = img.shape
        
        width  = img_shape[1]
        height = img_shape[0]
        depth  = img_shape[2]
            
        if self.crop:
            img = img[height//3 : height, 0 : width]  # cropping
                
        if self.resize:
            # img = cv2.resize(img, (0,0), fx=0.4, fy=1.2)  # resizing
            img = cv2.resize(img, (0,0), fx=self.fx, fy=self.fy)  # resizing
            
        if self.flip:
            img = cv2.flip(img, 1)
            
        if self.normalize:
            img = img / 255.0  # normalization
            
        if self.grayscale:
            img = np.expand_dims(img, axis=2) # if grayscaled
            
        return img