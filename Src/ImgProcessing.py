import cv2
import numpy as np
from tensorflow.keras.preprocessing.image import ImageDataGenerator


class ImgProcessor:
    
    def __init__(self):

        self.grayscale = False
        self.flip = False
        self.resize = True
        self.crop = True
        self.normalize = True
        self.augment = False
        
        self.fx = 0.4
        self.fy = 1.2
        
        self.augmentor = ImageDataGenerator(rotation_range=5, width_shift_range=0.1, height_shift_range=0.1)
    
    def process(self, img):
        
        if self.grayscale:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # grayscaling
            img = img[:, :, np.newaxis]
                
        img_shape = img.shape
        
        width  = img_shape[1]
        height = img_shape[0]
        depth  = img_shape[2]
            
        if self.crop:
            img = img[height//3 : height, 0 : width]  # cropping
                
        if self.resize:
            # img = cv2.resize(img, (0,0), fx=0.4, fy=1.2)  # resizing
            img = cv2.resize(img, (0,0), fx=self.fx, fy=self.fy)  # resizing
            
        if self.flip:
            img = cv2.flip(img, 1)
            
        if self.normalize:
            img = img / 255.0  # normalization
            
        if self.grayscale:
            img = np.expand_dims(img, axis=2) # if grayscaled
            
        if self.augment:
            img = self.augmentor.flow(np.asarray([img]), batch_size=1, shuffle=False)[0][0]
            
        return img