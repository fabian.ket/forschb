#!/usr/bin/env python

import abc
from abc import abstractmethod

# compatible with Python 2 *and* 3:
ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()})

import Msg as msgs

class IMsgHandler(ABC):
    
    @abstractmethod
    def type(self):
        raise NotImplementedError("IMsgHandler type() not implemented")

    @abstractmethod
    def handle(self, msg):
        raise NotImplementedError("IMsgHandler handle() not implemented")


class PredictHandler(IMsgHandler):

    def __init__(self, model, batch_size=1):
        self._model = model
        self.batch_size = batch_size

    def type(self):
        return msgs.Msg.PREDICT

    def handle(self, msg):
        prediction = self._model.predict(msg.x, batch_size=self.batch_size)
        return msgs.PrecitionMsg(prediction)


class PredictionHandler(IMsgHandler):
    
    def __init__(self):
        pass

    def type(self):
        return msgs.Msg.PREDICTION

    def handle(self, msg):
        return None
