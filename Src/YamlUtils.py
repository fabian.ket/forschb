import yaml
from yaml import CLoader as Loader, CDumper as Dumper

def write_dict_to_yaml(file_name, dic):
    with open(file_name, 'w') as yaml_file:
        yaml.dump(dic, yaml_file, default_flow_style=False, Dumper=Dumper)
        
def read_yaml(file_name):
    result = {}
    
    with open(file_name, "r") as yaml_file:
        result = yaml.load(yaml_file, Loader=Loader)
        
    return result
    
    
def convert_neuro_data_to_yaml(file):
    result = {}
    
    yaml = read_yaml(file)
    
    for entry in yaml:
        
        img = entry["camera_messages"][0].split("/")[-1].split(".")[0]
        
        steering = entry["action_messages"]["/engine_wrapper/output/default"]["drive"]["steering_angle"]
        speed = entry["action_messages"]["/engine_wrapper/output/default"]["drive"]["speed"]
        
        result[img] = {"speed": speed, "steering": steering}
        
    return result