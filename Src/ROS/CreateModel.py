#!/usr/bin/env python

class CreateModel:
    
    def __init__(self, f_model, img_shape, batch_size, stateful, compile_model, weights_file):
        self.f_model       = f_model
        self.img_shape     = img_shape
        self.batch_size    = batch_size
        self.stateful      = stateful
        self.compile_model = compile_model
        self.weights_file  = weights_file

    def create_sequence_model(self):
        model = self.f_model(self.img_shape, self.batch_size, self.stateful, self.compile_model)
        model.load_weights(self.weights_file)
        return model

    def create_model(self):
        model = self.f_model(self.img_shape, self.batch_size, self.compile_model)
        model.load_weights(self.weights_file)
        return model
