import tensorflow.compat.v1.keras.callbacks as kc

class KerasRestStatesCallback(kc.Callback):
    def __init__(self, model):
        self._model = model
        print("Parameter model ref: {}".format(hex(id(model))))
        print("Callback  model ref: {}".format(hex(id(self._model))))
        
    # https://github.com/keras-team/keras/blob/master/keras/callbacks.py#L275
    def on_batch_end(self, batch, logs=None):
        pass
#         self._model.reset_states()
    def on_epoch_end(self, epoch, logs=None):
        self._model.reset_states()