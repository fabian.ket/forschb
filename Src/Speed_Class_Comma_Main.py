import numpy as np
import YamlUtils as yu
import DicData as dade
import Slideshow as slide
import os

import KerasSequence as ks
import ImgProcessing as img_proc
import Models as models
import KerasCallbacks as kc

def do_all(dic):
    dade.remove_data_with_speed_lower_than_threshold(dic)
    max_abs_speed, max_abs_steer = dade.get_max_abs_speed_and_steering(dic)
    dade.normalize_dic_steering(dic, max_abs_steer)
    # dade.normalize_dic_speed(dic, max_abs_speed)
    dade.add_speed_classes_to_dic(dic, threshold=0.05)
    dade.add_steer_classes_to_dic(dic)
    dade.add_speed_cost_weight_to_dic(dic)
    dade.add_steer_cost_weight_to_dic(dic)
    
    return max_abs_steer, max_abs_speed

    
def main():
    img_path = "Z:/ForschB/comma_ai_extracted/{}/imgs"
    train_path = "Z:/ForschB/comma_ai_extracted"
    comma_ai_train_yaml = "Z:/ForschB/comma_ai_extracted/{}/training.yaml"

    dirs = os.listdir(train_path)

    # YAMLS
    yamls = []

    for d in dirs:
        yamls.append(comma_ai_train_yaml.format(d))
    
    # DICTS
    dicts = []

    for y in yamls:
        dicts.append(yu.read_yaml(y))
    
    # DO_ALL()
    max_steers = []
    max_speeds = []

    for d in dicts:
        max_steer, max_speed = do_all(d)
        max_steers.append(max_steer)
        max_speeds.append(max_speed)
    
    # IMG_PROCESSOR
    img_processor = img_proc.ImgProcessor()
    img_processor.crop = False
    img_processor.fx = 0.2
    img_processor.fy = 0.4
    
    # TRAIN GENS
    batch_size = 1
    sequence_size = 48
    
    speed_gens = []

    for f, d in zip(dicts, dirs):
        speed_gens.append(
            ks.SequenceGeneratorSpeed(f, img_path.format(d), 
            img_processor, batch_size=batch_size, sequence_size=sequence_size, 
            img_file_type="png")
        )
    
    # MODELS
    ([imgs, _], [_], _) = speed_gens[0][0]
    img_shape = imgs[0][0].shape
        
    models.reset_session()
    
    sp_model = models.fabian_ket_lstm_research_b_speed_paper(
        img_shape, batch_size, stateful=True, 
        compile_model=True, optimizer="adam", bias=True
    )

    reset_states_callback = kc.KerasRestStatesCallback(sp_model)
    
    # TRAINING
    sp_model_save_file = "speed_model_weights_64x64_comma.ai_speed_paper()_adam_bias.h5"
    
    history = sp_model.fit_generator(
        generator=speed_gens[0],
        steps_per_epoch=len(speed_gens[0]),
        #validation_data=steer_gen11,
        #validation_steps=len(steer_gen11),
        epochs=10,
        verbose=1,
        workers=1,
        use_multiprocessing=False,
        callbacks=[reset_states_callback]
    )
    
    sp_model.save_weights( sp_model_save_file )
    
if __name__ == "__main__":
    main()