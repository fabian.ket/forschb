#!/usr/bin/env python

# ROS imports
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Joy
from geometry_msgs.msg import TwistStamped

# sys imports
import numpy as np
import Queue
import time
from collections import deque


# own imports
import Worker as work
import Models as m
import SequenceModels as sm
import CreateModel as cm
import ImgProcessing as img_proc
import Msg as msgs
import MsgHandler as handlers
import Speed as sp


class AiTwoModels:
    def __init__(self, verbose=0):
        self.t0 = time.time()
        
        self.verbose = verbose

        img_shape = (64, 64, 3)
        
        # speed_model_save_file = 'speed_eager_valid_model5_(0.12097287762822917).h5'
        # speed_model_save_file = 'speed_eager_valid_model6_(0.1792880365018557)_stateful.h5' das HIER war es
        # speed_model_save_file = 'speed_eager_valid_model6_(0.1479115190101038)_stateful_half.h5' 
        speed_model_save_file = 'speed_eager_valid_model6_both_in_speed_out_(0.13788395285377392)_stateless_half.h5' 

        # steer_model_save_file = 'steer_model_weights_64x64_sim_cnn_steer_paper()_adam_bias.h5'
        # steer_model_save_file = 'steer_eager_valid_model4_only_(0.07953199283425397).h5' # Das HIER
        # steer_model_save_file = 'steer_eager_valid_model6_both_in_steer_out_(0.06439242754528426)_stateless_half.h5'
        # steer_model_save_file = 'steer_eager_valid_model6_both_in_steer_out_(0.06962638367197563)_stateless_half.h5'
        # steer_model_save_file = 'steer_eager_valid_model6_both_in_steer_out_seq16_(0.08067094351016424)_stateless_half.h5' # HIER
        steer_model_save_file = 'steer_eager_valid_model6_both_in_steer_out_seq16_over_(0.09899718878482361)_stateless_half.h5' # einfaches verdoppeln

        # both_model_save_file = 'speed_eager_model6_both.h5'
        # both_model_save_file = 'speed_eager_valid_model6_both_(0.21411981259507656)_stateful_half.h5'
        # both_model_save_file = 'speed_eager_valid_model6_both_in_speed_out_(0.14442175838646118)_stateless_half.h5'
        # both_model_save_file = 'speed_eager_valid_model6_both_own_lstm_(0.21394709841361584)_stateless_half.h5'
        
        # creator_speed = cm.CreateModel(m.generator_model5, img_shape, 1, True, False, speed_model_save_file)
        # creator_speed = cm.CreateModel(m.generator_model6, img_shape, 1, True, False, speed_model_save_file)
        creator_speed = cm.CreateModel(sm.generator_model6_both_in_speed_out, img_shape, 1, True, False, speed_model_save_file)

        # creator_steer = cm.CreateModel(m.fabian_ket_cnn_research_b_steer_paper, img_shape, 1, True, False, steer_model_save_file)
        # creator_steer = cm.CreateModel(m.generator_model4_only_img_steer, img_shape, 1, True, False, steer_model_save_file)
        creator_steer = cm.CreateModel(sm.generator_model6_both_in_steer_out, img_shape, 1, True, False, steer_model_save_file)

        # creator_both = cm.CreateModel(sm.generator_model6_both_own_lstm, img_shape, 1, True, False, both_model_save_file)

        speed_model = creator_speed.create_model()
        steer_model = creator_steer.create_model()
        # both_model = creator_both.create_model()

        speed_model._make_predict_function()
        steer_model._make_predict_function()
        # both_model._make_predict_function()        

        self.speed_queue = Queue.Queue()
        self.steer_queue = Queue.Queue()
        # self.both_queue = Queue.Queue()

        self.speed_worker = work.Worker(self.speed_queue)
        self.steer_worker = work.Worker(self.steer_queue)
        # self.both_worker = work.Worker(self.both_queue)

        speed_handler = handlers.PredictHandler(speed_model)
        steer_handler = handlers.PredictHandler(steer_model)
        # both_handler = handlers.PredictHandler(both_model)

        self.speed_worker.register(speed_handler)
        self.steer_worker.register(steer_handler)
        # self.both_worker.register(both_handler)

        self.pre_speed = 0.0
        # self.pre_speed_class = np.asarray([1, 0, 0])
        # self.speed_qeue = deque([0 for _ in range(50)], maxlen=50)

        self.pre_steer = 0.0

        self.speed_worker.start()
        self.steer_worker.start()
        # self.both_worker.start()

        # ROS
        self.twist = TwistStamped()

        self.cv_bridge = CvBridge()

        self.img_processor = img_proc.ImgProcessor()
        self.img_processor.crop = True
        self.img_processor.fx = 0.1
        self.img_processor.fy = 0.2

        rospy.init_node('ai_two_models', anonymous=True)
        self.pub = rospy.Publisher('/nr/ai/output/actions', TwistStamped, queue_size=1)
        self.sub = rospy.Subscriber('/depth_camera/rgb/image_raw/compressed', CompressedImage, self.cam_callback, queue_size=1)
        
        self.joy_steer = 0.0
        self.sub_joy = rospy.Subscriber('/joy', Joy, self.joy_callback, queue_size=1)

    def cam_callback(self, msg):

        if self.verbose > 0:    
            print("{} Hz".format(1.0 / (time.time() - self.t0)))
        
        self.t0 = time.time()
        
        # prepare model inputs
        img = self.cv_bridge.compressed_imgmsg_to_cv2(msg, desired_encoding="passthrough")
        img = self.img_processor.process(img)
        
        # input_speed = np.asarray([ img ])
        input_speed = [ np.asarray([[ img ]]), np.asarray([[ [self.pre_speed] ]]), np.asarray([[ [self.pre_steer] ]]) ]

        # input_steer = np.asarray([ img ])
        input_steer = [ np.asarray([[ img ]]), np.asarray([[ [self.pre_speed] ]]), np.asarray([[ [self.pre_steer] ]]) ]
        # input_steer = [ np.asarray([[ img ]]), np.asarray([[ [self.pre_steer] ]]) ]
        
        # input_both = [ np.asarray([[ img ]]), np.asarray([[ [self.pre_speed] ]]), np.asarray([[ [self.pre_steer] ]]) ]

        # pass input to workers
        speed_msg = msgs.PredictMsg(input_speed)
        steer_msg = msgs.PredictMsg(input_steer)

        # both_msg = msgs.PredictMsg(input_both)

        self.speed_worker.put(speed_msg)
        self.steer_worker.put(steer_msg)
        # self.both_worker.put(both_msg)

        
        # process predictions
        speed_prediction_msg = self.speed_queue.get()
        steer_prediction_msg = self.steer_queue.get()
        # both_prediction_msg = self.both_queue.get()

        speed_pred = speed_prediction_msg.prediction
        steer_pred = steer_prediction_msg.prediction

        # [speed_pred, steer_pred] = both_prediction_msg.prediction

        speed_pred = np.asscalar(speed_pred)
        steer_pred = np.asscalar(steer_pred)

        # speed_pred_class = np.argmax(speed_pred[0])

        # if speed_pred_class == sp.Speed.ACCELERATE:
        #     self.pre_speed += 0.05
        # elif speed_pred_class == sp.Speed.DEACCELERATE:
        #     self.pre_speed -= 0.1
        # elif (speed_pred_class == sp.Speed.MAINTAIN or speed_pred_class == sp.Speed.DEACCELERATE) and self.pre_speed == 0.0:
        #     self.pre_speed += 0.1
        
        # self.speed_qeue.append(speed_pred)

        self.pre_speed = speed_pred

        # self.pre_speed = 0.1 if self.pre_speed <= 0.1 else self.pre_speed
        # self.pre_speed = 1.0 if self.pre_speed > 1.0 else self.pre_speed

        # steer_pred = np.asscalar(steer_prediction_msg.prediction)
        
        if self.verbose > 1:
            print("Speed: {} | Steer: {}".format(speed_pred, steer_pred))

        # self.pre_speed_class = speed_pred[0][0]
        self.pre_steer = steer_pred
        
        self.twist.twist.linear.x  = self.pre_speed # speed 
        self.twist.twist.angular.z = self.pre_steer # self.joy_steer # steering

        self.pub.publish(self.twist)
    
    def joy_callback(self, msg):
        self.joy_steer = -msg.axes[0]

    def exit(self):
        self.speed_worker.put(None)
        self.steer_worker.put(None)
        # self.both_worker.put(None)
        print("Threads closed")
    

def main():
    ai = AiTwoModels(verbose=1)
    rospy.spin()
    ai.exit()


if __name__ == "__main__":
    main()
