#!/usr/bin/env python

from threading import Thread
import Queue
import time


class Worker: 
    def __init__(self, output):
        
        self.msgs = Queue.Queue()
        self.thread = Thread(target=self.run, args=())
        
        self.output = output

        self.msg_handlers = {}
        
    def run(self):
        
        print("Worker started")

        while(True):
            msg = self.msgs.get()

            if msg is None:
                break
            
            result = self.msg_handlers[msg.type()].handle(msg)
            self.output.put(result)
    
    def put(self, msg):
        self.msgs.put(msg)
        
    def start(self):
        self.thread.start()
    
    def join(self):
        self.thread.join()
        
    def is_alive(self):
        return self.thread.is_alive()

    def register(self, handler):
        self.msg_handlers[handler.type()] = handler
