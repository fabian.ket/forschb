import numpy as np
import cv2
import keras.backend as K
import copy
import time
import os

# own imports
import Src.Models as models

def get_mask_neg_batch_uses_mat(width, height, mask_width=1, mask_height=1):
    
    mask_batch = []
    neg_mask_batch = []
    
    uses = np.zeros((height, width, 1))
    mask = np.ones_like(uses)
    
    for y in range(height - mask_height + 1):
        for x in range(width - mask_width + 1):
                        
            mask[:, :, :] = 1
            mask[(0+y) : (mask_height+y), (0+x) : (mask_width+x), :] = 0       
            mask_batch.append([copy.deepcopy(mask)])
            
            neg_mask = (1 - mask)
            neg_mask_batch.append([neg_mask])
            
            uses += neg_mask
    
    mask_batch = np.asarray(mask_batch)
    neg_mask_batch = np.asarray(neg_mask_batch)
    
    return mask_batch, neg_mask_batch, uses
    

def normalize_img(img):
    max_value = np.max(img)
    min_value = np.min(img)

    img -=  min_value
    img /= (max_value - min_value)
        
def heatmap(f_model, gen, save_dir="test/", batch_size=512, rnn_layer_name="lstm"):
    
    # create save dir
    t0 = time.time()
    save_dir = save_dir + "{}/".format(t0)
    os.makedirs(save_dir)
    
    # create model
    model = f_model()
    model.reset_states()
    model.set_internal_tensors()
    
    # sess = K.get_session()
    
    n_round = 0
    
    
    img_shape = model.get_img_shape()
    
    width  = img_shape[1]
    height = img_shape[0]
    depth  = img_shape[2]
    
    
    mask_batch, neg_mask_batch, uses = get_mask_neg_batch_uses_mat(width, height)
    
    length = width * height
    steps = length // batch_size
    
    img_batch_mask = np.ones((length, 1, height, width, depth))
    pre_batch_mask = np.ones((batch_size, 1, 1))
    
    print("Start heatmap gen ...")
    
    for img, pre_speed, pre_steer in gen:
                
        t0 = time.time()
        
        result = np.zeros_like(uses)
        
        img_batch = np.multiply(img_batch_mask, np.asarray([[img]]))
        mask_img_batch = np.multiply(img_batch, mask_batch)
        
        img_batch = img_batch[0:batch_size]
        
        pre_speed_batch = np.multiply(pre_batch_mask, np.asarray([[pre_speed]]))
        pre_steer_batch = np.multiply(pre_batch_mask, np.asarray([[pre_steer]]))
        
        t1 = time.time()
        
        print("start predictions ... ({}sec)".format(t1 - t0))
        
        # model.set_internal_values(sess)
        value_cell_tensor   = copy.deepcopy(model._cell_tensor.numpy())
        value_hidden_tensor = copy.deepcopy(model._hidden_tensor.numpy())
        
        # tensorflow session slows down after some calls
        # seems like jupyter is the reason
        # if (n_round % 10) == 0:
            # models.reset_session()
            # sess = K.get_session()
            
            # model.copy(f_model())
            # model.set_internal_tensors()
            # model.assign_internal_values(sess)
        
        # you could just predict with the whole batch at once,
        # but then the internal lstm states wouldn't be reseted by keras
        # so we have to do it by ourself
        predictions = None
        for i in range(steps):
            
            start = 0 + (batch_size * i)
            end   = batch_size + (batch_size * i)
            
            # model.assign_internal_values(sess)
            model._cell_tensor.assign(value_cell_tensor)
            model._hidden_tensor.assign(value_hidden_tensor)
            
            p = model.predict([ mask_img_batch[start : end], pre_speed_batch, pre_steer_batch ], 
                              batch_size=batch_size)
            
            if predictions is None:
                predictions = p
                
            else:
                predictions = np.concatenate( (predictions, p) )
        
        del mask_img_batch
        
        # set internal states for next sequence element
        # model.assign_internal_values(sess)
        model._cell_tensor.assign(value_cell_tensor)
        model._hidden_tensor.assign(value_hidden_tensor)
        
        p = model.predict([ img_batch, pre_speed_batch, pre_steer_batch ], batch_size=batch_size)
        
        del img_batch
        del pre_speed_batch
        del pre_steer_batch
        
        ground_truth = np.repeat(p, steps, axis=0)
        
        print("... predictions done ({}sec)".format(time.time() - t1))
        
        results = []
        
        for neg_mask_seq, ground_truth_seq, predictions_seq in zip(neg_mask_batch, ground_truth, predictions):
            tmp = np.multiply( neg_mask_seq[0], np.sum(np.abs(ground_truth_seq[0] - predictions_seq[0])) )
            results.append(tmp)
            
        results = np.asarray(results)

        
        results = np.sum(results, axis=0)
        results /= uses
        
        normalize_img(results)
        
        # to_display = cv2.resize(results, (400, 400))
        # cv2.imwrite("{}{}.png".format(save_dir, time.time()), (to_display * 255))
        
        
        results = cv2.resize(results, (320, 320))
        results = np.reshape(results, (320, 320, 1))
        
        to_save = np.zeros((320, 320 * 2, depth))
        to_save[:, 0:320, :] += results * 255
        to_save[:, 320: , :] += cv2.resize(img, (320, 320)) * 255
        
        cv2.imwrite("{}{}.png".format(save_dir, time.time()), to_save)
        
        
        print("Duration {}: {}sec".format(n_round, time.time() - t0))
        print("==================")
        
        n_round += 1
                
    