import csv
import numpy as np
import os
import copy

def read_uda_interpolated_csv_to_dict(file_name, cam_id="center_camera"):
    result = {}
    with open(file_name, mode="r") as csv_file:
        csv_reader = csv.DictReader(csv_file)
    
        for row in csv_reader:
            
            if row["frame_id"] == cam_id:
                timestamp = int(row["timestamp"])
                angle  = float(row["angle"])
                torque = float(row["torque"])
                speed  = float(row["speed"])
                
                result[timestamp] = {"angle": angle, "torque": torque, "speed": speed}
        
    return result

def read_uda_csv(file_name, key):
    
    result = []

    with open(file_name, mode="r") as csv_file:
        csv_reader = csv.DictReader(csv_file)
    
        for row in csv_reader:
            result.append( (int(row["timestamp"]), float(row[key])) )
        
    return result

def read_uda_image_timestamps(file_name):
    
    result = []
    
    images = os.listdir(file_name)
    for i in images:
        result.append(int(i[:-4]))
        
    return result
    
def create_uda_image_steer_and_speed_dict(timestamps, steerings, speeds):
    result = {}
    length = len(timestamps)
    
    steerings = copy.deepcopy(steerings)
    speeds = copy.deepcopy(speeds)
    
#     def get_values_smaller_threshold(l, threshold, acc):
        
#         if (len(l) <= 0):
#             return l, acc
        
#         if (l[0][0] >= threshold):
#             return l, acc
        
#         acc.append(l[0][1])
#         l.pop(0)
        
#         return get_values_lower_threshold(l, threshold, acc)

    def get_values_smaller_threshold(l, threshold):
        
        result = []
        
        for ele in l:
            
            if ele[0] >= threshold:
                break
                
            result.append(ele[1])
                
            
        return result
            
    
    for i, timestamp in enumerate(timestamps):
        acc = []
        steer = 0.0
        speed = 0.0
        
        if (i+1) < length:
            acc = get_values_smaller_threshold(steerings, timestamps[i+1])
            steer = -float(np.mean(acc))
            
            del steerings[:len(acc)]
            
            acc = []
            
            acc = get_values_smaller_threshold(speeds, timestamps[i+1])
            speed = float(np.mean(acc))
            
            del speeds[:len(acc)]
            
        else:
            if len(steerings) > 0:
                steer = -float(np.apply_along_axis(np.mean, 0, steerings)[1])
                
            if len(speeds) > 0:
                speed = float(np.apply_along_axis(np.mean, 0, speeds)[1])
            
        
        result[timestamp] = {"steering": steer, "speed": speed}
    
    return result
    