#!/usr/bin/env python

from threading import Thread
import Queue
import time


class PredictionWorker: 
    def __init__(self, model, output):
        
        self.msgs = Queue.Queue()
        self.thread = Thread(target=self.run, args=())
        
        self.model = model
        self.output = output
        
    def run(self):
        
        print("Worker started")

        while(True):
            msg = self.msgs.get()

            if msg is None:
                break
            
            prediction = self.model.predict(msg, batch_size=1)
            
            self.output.put(prediction)
    
    def put(self, msg):
        self.msgs.put(msg)
        
    def start(self):
        self.thread.start()
    
    def join(self):
        self.thread.join()
        
    def is_alive(self):
        return self.thread.is_alive()
