import math
import cv2
import numpy as np
import copy
import Src.Speed as Speed
import Src.DicData as dida

TRUE_STEER_COLOR = (0, 255, 0)    # label arrow color
TRUE_SPEED_COLOR = (255, 255, 0)  # prediction arrow color

PRED_STEER_COLOR = (0, 0, 255)
PRED_SPEED_COLOR = (255, 0, 0)

MIN_STEER_DEGREES = -90           # max left angle
MAX_STEER_DEGREES =  90           # max right angle

IMAGE_RESIZE_WIDTH = 800
    

def fmap(value, old_min, old_max, new_min, new_max):
    old_span = old_max - old_min
    new_span = new_max - new_min
    return (value - old_min) * new_span / old_span + new_min


def resize(img):
    width = img.shape[1]
    height = img.shape[0]

    height = height * IMAGE_RESIZE_WIDTH // width
    width = IMAGE_RESIZE_WIDTH
        
    return cv2.resize(img, (width, height))
    
    
def get_draw_coordinates(img, draw_border):
    width = img.shape[1]
    height = img.shape[0]
    
    draw_width  = int(width / 10)
    draw_height = int(height / 3)
    
    left   = width - draw_width - draw_border
    right  = width - draw_border
    middle = width - int(draw_width / 2) - draw_border
    
    top    = height - draw_height - draw_border
    bottom = height - draw_border
    
    return top, bottom, left, right, middle
    
    
def draw_speed_scale_on_img(img, draw_border=5):
    t, b, l, r, m = get_draw_coordinates(img, draw_border)
    
    WHITE = (255,255,255)
    
    # draw scale
    cv2.line(img, (m, b), (m, t), WHITE, 4)
    cv2.line(img, (l, t), (r, t), WHITE, 4)
    cv2.line(img, (l, b), (r, b), WHITE, 4)

    
def draw_steer_scale_on_img(img):
    image_width = img.shape[1]
    image_height = img.shape[0]
    image_hor_center = int(image_width / 2)
    arrow_origin = (image_hor_center, image_height)
    arrow_length = image_height / 2
    
    radius = int(arrow_length)
    axes = (radius, radius)
    startAngle = 0.0;
    endAngle = -180.0;
    WHITE = (255, 255, 255)
    cv2.ellipse(img, arrow_origin, axes, 0.0, startAngle, endAngle, WHITE, 2)
    
    
def draw_speed_class_on_img(img, speed_class, color, to_right=3, to_top=20, draw_border=5):
    t, b, l, r, m = get_draw_coordinates(img, draw_border)

    text = ""

    if speed_class == Speed.Speed.MAINTAIN:
        text = "MAIN"
    
    elif speed_class == Speed.Speed.ACCELERATE:
        text = "ACC"
        
    else:
        text = "DEC"

    cv2.putText(img, text, (l + to_right, t - to_top), cv2.FONT_HERSHEY_PLAIN, 2, color, 2)
    
    
def draw_speed_line_on_img(img, speed, color, draw_border=5):
    t, b, l, r, m = get_draw_coordinates(img, draw_border)
    
    # draw speed line
    speed_height = b - int((b - t) * speed)
    cv2.line(img, (l, speed_height), (r, speed_height), color, 4)

    
def draw_steer_arrow_on_img(img, angle, color):
    image_width = img.shape[1]
    image_height = img.shape[0]
    image_hor_center = int(image_width / 2)
    arrow_origin = (image_hor_center, image_height)
    arrow_length = image_height / 2

    steer_angle_rad = math.radians(angle)

    arrow_end = (
        image_hor_center + int(arrow_length * math.sin(steer_angle_rad)),
        image_height - int(arrow_length * math.cos(steer_angle_rad)))
    
    cv2.arrowedLine(img, arrow_origin, arrow_end, color, 4)
    
    
def get_dic_entry_values(dic_entry):
    return dic_entry["steering"], dic_entry["speed"], dic_entry["steering_class"], dic_entry["speed_class"]
    
    
def prepare_value_for_model(value, sequential):
    if sequential:
        return np.asarray( [[value]] )
    else:
        return np.asarray( [value] )
        
        
def slideshow(dic, img_path, 
    minimal_steer=-1.0, maximal_steer=1.0, 
    minimal_speed=0.0, maximal_speed=1.0,
    img_processor=None,
    speed_model=None,
    steer_model=None,
    speed_model_reg=None,
    both_reg_model=None,
    sequential=True,
    hz=50,
    img_file_type="png",
    verbose=0):

    cv2.namedWindow('Slideshow', cv2.WINDOW_AUTOSIZE)
    
    ms = int(1000 / hz)
    
    pre_key = None
    pre_steer = 0.0 # np.zeros(13)
    pre_speed = np.zeros(3)
    pre_speed_reg = 0.0
    
    for key in dic.keys():
        
        # LOAD IMG ...
        
        img_name = key
        if "img_name" in dic[key].keys():
            img_name = dic[key]["img_name"]
            
        path = img_path
        if "img_path" in dic[key].keys():
            path = dic[key]["img_path"]
        
        file_name = "{}/{}.{}".format(path, img_name, img_file_type)
        
        img = cv2.imread(file_name)
        
        if speed_model is not None or steer_model is not None or speed_model_reg is not None or both_reg_model is not None:
            processed_img = copy.deepcopy(img)
            processed_img = img_processor.process(processed_img)
        
        img = resize(img)
        # ... LOAD IMG
        
        # DRAW SCALES ...
        draw_speed_scale_on_img(img)
        draw_steer_scale_on_img(img)
        # ... DRAW SCALES
        
        steer, speed, steer_class, speed_class = get_dic_entry_values(dic[key])
        
        steer_values = dida.get_steer_values_dic()
        steer_class_value = steer_values[np.argmax(steer_class)]
        
        steer_norm = fmap(steer, minimal_steer, maximal_steer, MIN_STEER_DEGREES, MAX_STEER_DEGREES)
        speed_norm = fmap(speed, minimal_speed, maximal_speed, 0, 1)
        
        
        # DRAW TRUE VALUES ...
        draw_steer_arrow_on_img(img, steer_norm, TRUE_STEER_COLOR)
        draw_speed_line_on_img(img, speed_norm, TRUE_SPEED_COLOR)
        draw_speed_class_on_img(img, np.argmax(speed_class), TRUE_SPEED_COLOR, to_top=20)
        
        cv2.putText(img, "Steer: {}".format(steer), (5, 20), cv2.FONT_HERSHEY_PLAIN, 1, TRUE_STEER_COLOR, 2)
        cv2.putText(img, "Speed: {}".format(speed), (5, 40), cv2.FONT_HERSHEY_PLAIN, 1, TRUE_SPEED_COLOR, 2)
        # ... DRAW TRUE VALUES
        
        
        if speed_model is not None:
            
            # if pre_key is not None:
            #    pre_speed = dic[pre_key]["speed_class"]
            
            tmp = prepare_value_for_model(processed_img, sequential)
            pre = prepare_value_for_model(pre_speed, sequential)
            
            l = [tmp, pre]
            if not sequential:
                l = [tmp]
            
            pred_speed = speed_model.predict( l, batch_size=1 )
            pred_speed = pred_speed[0][0]
            
            pre_speed = np.zeros_like(pred_speed)
            
            argmax = np.argmax(pred_speed)
            pre_speed[argmax] = 1.0
            
            draw_speed_class_on_img(img, argmax, PRED_SPEED_COLOR, to_top=50)
        
        if speed_model_reg is not None:
        
            test_img0 = np.zeros_like(processed_img)
            test_img1 = np.ones_like(processed_img)
            test_speed = 0.0
        
            tmp = prepare_value_for_model(processed_img, sequential)
            pre = prepare_value_for_model([pre_speed_reg], sequential)
            
            l = [tmp, pre]
            if not sequential:
                l = [tmp]
            
            pred_speed = speed_model_reg.predict( l, batch_size=1 )
            pred_speed = np.asscalar(pred_speed)
            
            pred_speed_norm = fmap(pred_speed, minimal_speed, maximal_speed, 0, 1)
            
            draw_speed_line_on_img(img, pred_speed_norm, PRED_SPEED_COLOR)
            
            pre_speed_reg = pred_speed
        
        if steer_model is not None:
            
            # if pre_key is not None:
            #     pre_steer = dic[pre_key]["steering"]
            
            # if False:
            #     processed_img = np.zeros_like(processed_img)
            
            tmp = prepare_value_for_model(processed_img, sequential)
            pre = prepare_value_for_model([pre_steer], sequential)
            
            l = [tmp, pre]
            if not sequential:
                l = [tmp]
            
            pred_steer = steer_model.predict( l, batch_size=1 )            
            pred_steer = np.asscalar(pred_steer)
            
            # pre_steer = np.zeros_like(pred_steer)
            
            # argmax = np.argmax(pred_steer)
            # pre_steer[argmax] = 1.0
            
            pre_steer = pred_steer
            
            if verbose >= 1:
                print("steer_model prediction: {}".format(pred_steer))
            
            pred_steer = fmap(pred_steer, -1, 1, MIN_STEER_DEGREES, MAX_STEER_DEGREES)
            draw_steer_arrow_on_img(img, pred_steer, PRED_STEER_COLOR)
            
            
        if both_reg_model is not None:
        
            test_img0 = np.zeros_like(processed_img)
            test_img1 = np.ones_like(processed_img)
            test_speed = 0.0
        
            tmp = prepare_value_for_model(processed_img, sequential)
            pre_sp = prepare_value_for_model([pre_speed_reg], sequential)
            pre_st = prepare_value_for_model([pre_steer], sequential)
            
            l = [tmp, pre_sp, pre_st]
            if not sequential:
                l = [tmp]
            
            [pred_sp, pred_st] = both_reg_model.predict( l, batch_size=1 )
            pred_sp = np.asscalar(pred_sp)
            pred_st = np.asscalar(pred_st)
            
            pred_sp_norm = fmap(pred_sp, minimal_speed, maximal_speed, 0, 1)
            draw_speed_line_on_img(img, pred_sp_norm, PRED_SPEED_COLOR)
            
            pred_st_norm = fmap(pred_st, -1, 1, MIN_STEER_DEGREES, MAX_STEER_DEGREES)
            draw_steer_arrow_on_img(img, pred_st_norm, PRED_STEER_COLOR)
            
            pre_speed_reg = pred_sp
            pre_steer = pred_st
        
        pre_key = key
        
        cv2.imshow("Slideshow", img)
        
        if verbose >= 2:
            print("Key: {}".format(key))
        
        k = cv2.waitKey(ms)

        if k == 27:  # esc
            break
                
    cv2.destroyAllWindows()

    
# used with Keras.Utils.Sequence
def show_data_gen_imgs(data_gen):
    cv2.namedWindow('Slideshow', cv2.WINDOW_AUTOSIZE)

    for ([img_batch, pre_speed_batch], [steer_batch, speed_batch]) in data_gen:

        for img_seq, steer_seq, speed_seq in zip(img_batch, steer_batch, speed_batch):

            for img_ele, steer_ele, speed_ele in zip(img_seq, steer_seq, speed_seq):
                cv2.imshow("Slideshow", img_ele)
                k = cv2.waitKey(20)

                if k == 27:  # esc
                    cv2.destroyAllWindows()
                    return
                    

    cv2.destroyAllWindows()
    
    
    
def imshow(img):
    cv2.imshow("img", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()