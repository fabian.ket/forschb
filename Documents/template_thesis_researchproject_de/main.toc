\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Zielsetzung}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Vorgehensweise}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Recurrent Neural Network RNN}{3}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}RNN States}{4}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Class Imbalance Problem}{5}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Oversampling bei Timeseries}{7}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Heatmap zur Visualiesierung von Pixel of Interest}{11}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Zero-Padding Sequence Learning}{13}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Batch RNN Learning}{15}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.8}Second Hand Teacher Forcing}{17}{section.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.9}Profeccor Forcing}{21}{section.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Analyse}{22}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}NeuroRace Projekt}{22}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Simulation}{22}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Bisherige Steuerung}{23}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Heatmaps}{25}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Trainingsdaten}{26}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Konzeption}{28}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Steuerung des Fahrzeugs}{28}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Training der neuronalen Netze}{29}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Architektur der neuronalen Netze}{30}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Implementierung}{35}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Heatmap}{35}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Second Hand Teacher Forcing}{38}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Test}{40}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Testsystem}{40}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Fahrtests}{40}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Evaluation}{42}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Geschwindigkeitsklassifizierung}{42}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}RNN States}{43}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Second Hand Teacher Forcing}{45}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Oversampling}{45}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}Augmentierung}{46}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.6}Neuronale Netze}{47}{section.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.7}Sequenzl\IeC {\"a}nge und Frequenz}{48}{section.7.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.8}Batch Learning}{49}{section.7.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Fazit}{50}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Zusammenfassung}{50}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Kritischer R\IeC {\"u}ckblick}{52}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Ausblick}{52}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{54}{chapter*.26}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Tabellenverzeichnis}{55}{chapter*.27}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Source Code Content}{56}{chapter*.28}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{57}{chapter*.29}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Onlinereferenzen}{58}{chapter*.30}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bildreferenzen}{59}{chapter*.31}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Anhang}{60}{chapter*.31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Gazebo Teststrecke}{60}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Eigenst\IeC {\"a}ndigkeitserkl\IeC {\"a}rung}{61}{chapter*.34}
